<?php /* Template Name: Privacy-policy */ 
	get_header( 'home' );
?>
								
								<div class="container">
										<div class="company-logo-top">
												<img src="../../dc411ibrlpprl.cloudfront.net/company-logo-top.jpg" alt="Matrix" id="tag" />										</div>

										
										<div class="visible-sm-12 menu-phone top-link">
												<div class="col-sm-12">
														<span><a href="track-my-order.html">Track My Order</a></span> |
														<span><a href="contact-us.html">Contact Us</a></span>
												</div>
												<div class="clearfix center-block select-container">
														<div class="pull-left">
																																<form  method="post">
																		<select id="currchangePhone" name="ChangeCurrency" onchange="this.form.submit()">
																				<option value="USD" selected='selected'>USD</option>
																				<option value="EUR" >EUR</option>
																				<option value="GBP" >GBP</option>
																				<option value="AUD" >AUD</option>
																				<option value="MXN" >MXN</option>
																				<option value="CAD" >CAD</option>
																		</select>
																</form>
														</div>
														<div class="pull-left">
																<select id="changLangPhone">
																		<option value='https://www.phen375.com/en/privacy-policy.html' selected='selected'>English</option>
																		<option value='https://www.phen375.com/fr/privacy-policy.html' >Francais</option>
																<!--Nestrix | Ing. Franco Salas-->
																		<!--Set Deutsch Language-->
																		<option value='https://www.phen375.com/de/privacy-policy.html' >Deutsch</option>
																		<!--Set Greek Language-->
																		<option value='https://www.phen375.com/el/privacy-policy.html' >Ελληνική</option>
																		<!--Set Spanish Language-->
																		<option value='https://www.phen375.com/es/privacy-policy.html' >Español</option>
																		<!--Set Italian Language-->
																		<option value='https://www.phen375.com/it/privacy-policy.html' >Italiano</option>
																		<!--Set Dutch Language-->
																		<option value='https://www.phen375.com/nl/privacy-policy.html' >Dutch</option>
																<!--End Modifications-->
																</select>
														</div>
												</div>
										</div>

										<div class="containerMenuMobile overContentRelative">
											
																						<div id="menuMobile" class="overContent"></div>

											<!-- <div class="fullWidth fullHeight overContentRelative">
												<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
												</div>
											</div> -->
											<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
																									<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
														<tr>
															<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
															</td>
														</tr>
													</table>
																							</div>
											<a href="http://www.maxslim.store/shop" class="button inlineBlock overContent overContent-topRight containerMenuMobile-btnOrderNow">Order Now</a>										</div>

										
								</div>
						</div>
						<!-- End Header -->

						<!-- Content -->
						
						<link rel="canonical" href="privacy-policy.html" />
<div class="wrapper">
         <!-- content -->
         <div class="container content">
            <div class="container">
               <div class="row-new"><div class="col-md-12 feature-text">

		            <h1>Privacy policy</h1>
                  <p><span style="font-weight: bold;">What information do we collect?</span></p>
                     <ul>
                        <li>
                           <p>We collect information from you when you register on the site, place an order, enter a contest or sweepstakes, respond to a survey or communication such as e-mail, or participate in another site feature.</p>
                        </li>
                        <li>
                           <p>When ordering or registering, we may ask you for your name, e-mail address, mailing address, phone number, credit card information or other information. You may, however, visit our site anonymously.</p>
                        </li>
                        <li>
                           <p>We also collect information about gift recipients so that we can fulfill the gift purchase. The information we collect about gift recipients is not used for marketing purposes.</p>
                        </li>
                        <li>
                           <p>Like many websites, we use &quot;cookies&quot; to enhance your experience and gather information about visitors and visits to our website. Please refer to the &quot;Do we use 'cookies'?&quot; section below for information about cookies and how we use them.</p>
                        </li>
                     </ul>

                     <p><span style="font-weight: bold;">How do we use your information?</span></p>

                     <p>We may use the information we collect from you when you register, purchase products, enter a contest or promotion, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>
                     <ul>
                        <li>
                           <p>To personalize your site experience and to allow us to deliver the type of content and product offerings in which you are most interested.</p>
                        </li>
                        <li>
                           <p>To allow us to better service you in responding to your customer service requests.</p>
                        </li>
                        <li>
                           <p>To quickly process your transactions.</p>
                        </li>
                        <li>
                           <p>To administer a contest, promotion, survey or other site feature.</p>
                        </li>
                        <li>
                           <p>If you have opted-in to receive our e-mail newsletter, we may send you periodic e-mails. If you would no longer like to receive occasional&nbsp;e-mail from us, please refer to the "How can you opt-out, remove or modify information you have provided to us?" section below. If you have not opted-in to receive e-mail newsletters, you will not receive these e-mails. Visitors who register or participate in other site features such as marketing programs and 'members-only' content will be given a choice as to whether they would like to be on our e-mail list and receive e-mail communications from us.</p>
                        </li>
                     </ul>
                  <p><span style="font-weight: bold;">How do we protect visitor information?</span></p>

                  <p>We implement a variety of security measures to maintain the safety of your personal information. Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. When you place orders or access your personal information, we offer the use of a secure server. All sensitive/credit information you supply is transmitted via Secure Socket Layer (SSL) technology and then encrypted into our databases to be accessed only&nbsp;as stated above.</p>

                  <p><span style="font-weight: bold;">Do we use "cookies"?</span></p>
                  <p>Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>

                  <p>We may contract with third-party service providers to assist us in better understanding our site visitors. These service providers are not permitted to use the information collected on our behalf except to help us conduct and improve our business.</p>

                  <p>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser (like Netscape Navigator or Internet Explorer) settings. Each browser is a little different, so look at your browser Help menu to learn the correct way to modify your cookies. If you turn cookies off, you won't have access to many features that make your site experience more efficient and some of our services will not function properly.</p>

                  <p><span style="font-weight: bold;">Do we disclose the information we collect to outside parties?</span></p>

                  <p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information unless we provide you with advance notice, except as described below. The term &quot;outside parties&quot; does not include RDK Global. It also does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property, or safety.</p>

                  <p>However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>

                  <p><span style="font-weight: bold;">How can you opt-out, remove or modify information you have provided to us?</span></p>

                  <p>To modify your e-mail subscriptions, please use unsubscribe link in the email footer. Please note that due to email production schedules you may receive any emails already in production.</p>

                  <p>To delete all of your online account information from our database, please contact us at support@phen375.com with request and we will remove your details within next 48 hours.</p>

                  <p><span style="font-weight: bold;">Third-party links</span></p>
                  <p>In an attempt to provide you with increased value, we may include third party links on our site. These linked sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these linked sites (including if a specific link does not work).</p>

                  <p>Changes to our policy<span style="font-weight: bold;"></span></p>

                  <p>If we decide to change our privacy policy, we will post those changes on this page. Policy changes will apply only to information collected after the date of the change.</p>

                  <p><span style="font-weight: bold;">This policy was last modified on November 20, 2015.</span></p>
                  <p><span style="font-weight: bold;">Questions and feedback</span></p>
                  <p>We welcome your questions, comments, and concerns about privacy. Please send us any and all feedback pertaining to privacy, or any other issue.</p>

                  <p><span style="font-weight: bold;">Online policy only</span></p>
                  <p>This online privacy policy applies only to information collected through our website and not to information collected offline.</p>

                  <p><span style="font-weight: bold;">Terms and conditions</span></p>
                  <p>Please also visit our <a title="Phen375 Fat Burner" href="404.html">Terms and Conditions</a> section establishing the use, disclaimers, and limitations of liability governing the use of our website.</p>
                  <p>Your consent<span style="font-weight: bold;"></span></p>
                  <p>By using our site, you consent to our privacy policy.</p>

               </div></div>

               <!-- /.feature-text -->
            </div>
            <hr>
            <div class="row-new2 row-none"> <div class="row last-banner">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <img src="http://www.maxslim.store/wp-content/uploads/2018/05/blot.png" class="img-responsive center-block" alt="Banner Image">
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h2 class="blue font40">
                    <span class="special-font extra-bold block-span">START YOUR JOURNEY TODAY! TRY PHEN375!</span>
                </h2>
                <h3 class="bold">No Prescription Required!</h3>
                <a href="http://www.maxslim.store/shop" class="button button-links">Rush My Order</a>
                <div style="margin-top: 30px;">
                    <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                        <img name="trustseal" alt="Security Seals" style="border: 0; width: 120px;" src="<?php echo get_theme_file_uri(); ?>/assets/def/img/6121-lg.gif" />
                    </a>
                </div>

                <span class="block-span">
                                      </span>
            </div>
           <!--  <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padd">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/fad.png" class="img-responsive seal-img" alt="Made in a FDA Approved Facility">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in a FDA</span>
                        <span class="dark-blue block-span special-font bold">Approved Facility</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img2.png" class="img-responsive seal-img" alt="100% Quality Guaranteed">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">100% Quality</span>
                        <span class="dark-blue block-span special-font bold">Guaranteed</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img3.png" class="img-responsive seal-img" alt="Made in USA">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in</span>
                        <span class="dark-blue block-span special-font bold">USA</span>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4 col-xs-12">
            <div class="row">
                <!-- <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-6 seals">
                                            <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
                            <tr>
                                <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
                                </td>
                            </tr>
                        </table>
                                    </div> -->

                <!-- <div class="col-md-12 col-sm-12 col-xs-1 seals">
                    <center>
                        <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                            <img name="trustseal" alt="Security Seals" style="border: 0; width: 160px;" src="//dw26xg4lubooo.cloudfront.net/seals/stacked/6121-lg.gif" />
                        </a>
                    </center>
                </div> -->
            </div>
        </div>
</div></div>
            <div class="spacer"></div>
         </div>
      </div>						<!-- End Content -->

<?php get_footer( 'home' ); ?>