<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings           = array(
  'menu_title'      => 'Theme options',
  'menu_type'       => 'menu', // menu, submenu, options, theme, etc.
  'menu_slug'       => 'cs-framework',
  'ajax_save'       => true,
  'show_reset_all'  => false,
  'framework_title' => 'Theme options <small>by Codestar</small>',
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();
/***
 *  Custom theme options style starts here
 */
$options[] = array(
  'name'    => 'header_options',
  'title'   => 'Theme Options',
  'icon'    => 'fa fa-header',
  'fields'  => array(
    array(
      'type'    => 'notice',
      'class'   => 'success',
      'content' => 'Header part:',
    ),
    
    array(
      'id'      => 'header_list_icon',
      'type'    => 'group',
      'title'   => 'Header text with icon',
      'desc'    => 'Header right area text with icon. this will show on header. On the right side of the logo.',
      'button_title'  => 'Add item',
      'accordion_title' => 'Add Icon list for header',
      'fields'       => array(
        array(
          'id'    => 'header_icon_select',
          'type'  => 'select',
          'title' => 'Icon for the text',
          'options' => array(
            'icon'  => 'Icon',
            'image' => 'Image'
          )
        ),
        array(
          'id'    => 'header_icon',
          'type'  => 'icon',
          'title' => 'Icon for the text',
          'dependency'  => array('header_icon_select','==','icon'),
        ),
        array(
          'id'    => 'header_image',
          'type'  => 'image',
          'title' => 'Upload Image for list',
          'dependency'  => array('header_icon_select','==','image'),
        ),
        array(
          'id'    => 'header_text',
          'type'  => 'text',
          'title' => 'First line text',
        ),
        array(
          'id'    => 'header_text_second',
          'type'  => 'text',
          'title' => 'Second line text',
        ),
      ),
    ),
    
    array(
      'type'    => 'notice',
      'class'   => 'success',
      'content' => 'Footer Bottom Part',
    ),
    array(
      'id'    => 'footer_bottom_one',
      'type'  => 'wysiwyg',
      'title' => 'Footer Copyright Left',
    ),
    array(
      'id'    => 'footer_bottom_two',
      'type'  => 'wysiwyg',
      'title' => 'Footer Copyright Right',
    ),
     // woocommerce options
    array(
      'type'    => 'notice',
      'class'   => 'success',
      'content' => 'Woocommerce options',
    ),
    array(
      'id'    => 'add_to_cart_image',
      'type'  => 'upload',
      'title' => 'Woocommerce add to cart image',
      'default' => get_theme_file_uri( 'assets/images/select.png' ),
    ),
    array(
      'id'    => 'product_shiping_static',
      'type'  => 'text',
      'title' => 'Shiping Cost Static',
      'default' => '19.95',
    ),
    array(
      'id'    => 'checkout_page_title',
      'type'  => 'textarea',
      'title' => 'Checkout Title',
      'default' => 'CONGRATULATIONS, A NEW, SEXIER YOU IS AROUND THE CORNER!',
    ),
    array(
      'id'    => 'shop_page_title',
      'type'  => 'wysiwyg',
      'title' => 'Checkout Title',
      'default' => '<p style="font-size: 31px;"><span>Start Your Weight Loss Journey Today</span> -
            Order Phen375        </p>',
    ) 
  ),
);
/* custom style ends here */







CSFramework::instance( $settings, $options );
