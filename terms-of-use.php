<?php /* Template Name: Terms-of-use */
	get_header( 'home' );
 ?>
								
								<div class="container">
										<div class="company-logo-top">
												<img src="../../dc411ibrlpprl.cloudfront.net/company-logo-top.jpg" alt="Matrix" id="tag" />										</div>

										
										<div class="visible-sm-12 menu-phone top-link">
												<div class="col-sm-12">
														<span><a href="track-my-order.html">Track My Order</a></span> |
														<span><a href="contact-us.html">Contact Us</a></span>
												</div>
												<div class="clearfix center-block select-container">
														<div class="pull-left">
																																<form  method="post">
																		<select id="currchangePhone" name="ChangeCurrency" onchange="this.form.submit()">
																				<option value="USD" selected='selected'>USD</option>
																				<option value="EUR" >EUR</option>
																				<option value="GBP" >GBP</option>
																				<option value="AUD" >AUD</option>
																				<option value="MXN" >MXN</option>
																				<option value="CAD" >CAD</option>
																		</select>
																</form>
														</div>
														<div class="pull-left">
																<select id="changLangPhone">
																		<option value='https://www.phen375.com/en/terms-of-use.html' selected='selected'>English</option>
																		<option value='https://www.phen375.com/fr/terms-of-use.html' >Francais</option>
																<!--Nestrix | Ing. Franco Salas-->
																		<!--Set Deutsch Language-->
																		<option value='https://www.phen375.com/de/terms-of-use.html' >Deutsch</option>
																		<!--Set Greek Language-->
																		<option value='https://www.phen375.com/el/terms-of-use.html' >Ελληνική</option>
																		<!--Set Spanish Language-->
																		<option value='https://www.phen375.com/es/terms-of-use.html' >Español</option>
																		<!--Set Italian Language-->
																		<option value='https://www.phen375.com/it/terms-of-use.html' >Italiano</option>
																		<!--Set Dutch Language-->
																		<option value='https://www.phen375.com/nl/terms-of-use.html' >Dutch</option>
																<!--End Modifications-->
																</select>
														</div>
												</div>
										</div>

										<div class="containerMenuMobile overContentRelative">
											
																						<div id="menuMobile" class="overContent"></div>

											<!-- <div class="fullWidth fullHeight overContentRelative">
												<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
												</div>
											</div> -->
											<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
																									<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
														<tr>
															<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
															</td>
														</tr>
													</table>
																							</div>
											<a href="http://www.maxslim.store/shop" class="button inlineBlock overContent overContent-topRight containerMenuMobile-btnOrderNow">Order Now</a>										</div>

										
								</div>
						</div>
						<!-- End Header -->

						<!-- Content -->
						
						<link rel="canonical" href="terms-of-use.html" />
<div class="wrapper">
	<div class="container content">
		<div class="container">
			<div class="row-new"><div class="col-md-12 feature-text">

				<h1>Terms and Conditions</h1>
				<h2>Acceptance of Terms of Use</h2>

				<p>By using, visiting, or browsing the Site, you accept, without limitation or qualification, these Terms of Use and agree, without limitation, to the terms of our Privacy Statement. If you do not agree to these Terms of Use and Privacy Statement, please do not use our services or visit our Site. These Terms of Use constitute the entire agreement between Phen375 and you, and supersedes all prior or contemporaneous agreements, representations, warranties, and understandings with respect to the Site, the content, products, or services provided by or through the Site, and the subject matter of these Terms of Use.</p>
				<h2>Changes to the Terms of Use</h2>
				<p>Phen375 reserves the right, at its discretion, to change, modify, add, or remove portions of these terms at any time. You are bound by such revisions and should therefore periodically visit this page to determine the then current Terms of Use to which you are bound. You can review the most current version of the Terms of Use at any time by clicking on the “Terms of Use” hyperlink located at the bottom of the pages on the Site. By using the Site, you signify your acceptance of this Terms of Use. Your use of the Site after changes are made signifies your assent to be bound by the Terms of Use and Privacy Statement as they exist at that time.</p>
				<h2>Intellectual Property</h2>
				<p>The Site and all of its contents including, but not limited to, articles, other text, photographs, illustrations, graphics, product names, designs, logos, and the collection, arrangement, and assembly of all content (collectively, “the Intellectual Property”) are protected by copyright, trademark, and other laws of the United States and European Union , as well as international conventions and the laws of other countries. The Intellectual Property is the exclusive property of Phen375 and Shippitsa ltd.</p>
				<h2>Copyrights</h2>
				<p>The copyright in all materials provided on the Site is owned by Phen375 / Shippitsa ltd.
				Subject to the following exception, none of the material contained in the Site may be copied, reproduced, distributed, republished, downloaded, displayed, posted or transmitted in any form or by any means including, but not limited to, electronic, mechanical, photocopying, recording or otherwise, without the prior written consent of Phen375. Site visitors may only view, copy, print and download the materials on the Site for personal, non-commercial use only, provided such materials are used for informational purposes only, and all copies, or portions thereof, include this copyright notice. Phen375 may revoke any of the foregoing rights at any time. Upon termination of any rights granted here-under, you must immediately destroy any downloaded and printed materials. Any unauthorized use of any material contained on the Site may violate copyright laws, trademark laws, the laws of privacy and publicity, and communications regulations and statutes.</p>
				<h2>Trademarks</h2>
				<p>The trademarks, service marks and logos (“Trademarks”) used and displayed on the Site are either registered or unregistered Trademarks of Phen375. Nothing on the Site shall be construed as granting, by implication, estoppel, or otherwise any license or right to use any Trademark displayed on the Site without the prior written consent of the Trademark owner. The name Phen375 or any of the Trademarks may not be used in any way including in any advertising or publicity pertaining to distribution of materials on the Site without the prior written consent of Phen375. Phen375 prohibits use of its name or logo and Trademarks as a “hot” link to any non-Phen375 Site, unless establishment of such link is approved in advance by Phen375 in writing. Your use of the Site does not provide you with ownership rights to any Intellectual Property viewed through the Site nor does it waive any of Phen375’s rights in such information and materials. Phen375 will aggressively enforce its intellectual property rights to the fullest extent of the law.</p>
				<h2>Limited Right to Use</h2>
				<p>The viewing, printing or downloading of any content, graphic, form, or document from the Site grants you only a limited, non-exclusive license for use solely by you for your own personal, non-commercial use. You may not modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or offer for sale any information contained on, or obtained from, the Site. Illegal and/or unauthorized uses of the Site, including collecting usernames and/or email addresses of members by electronic or other means for the purpose of sending unsolicited commercial email; using any information retrieval system, whether electronic or through other means, to reproduce any of the content of the Site other than for your personal use; and unauthorized framing or linking to the Site will be investigated and appropriate legal action will be taken, including civil, criminal, and injunctive redress.</p>
				<h2>Site User Conduct</h2>
				<p>You must be 18 years of age or older to access our Site. As a user of the Site, you agree that in connection with your use of the Site and the content you will not:</p>
				<ul>
					<li>Upload, post, email, or otherwise transmit any content that is unlawful, harmful, threatening, abusive, harassing, tortuous, defamatory, vulgar, obscene, pornographic, libelous, invasive of anyone’s privacy, hateful, or racially, ethnically, or otherwise objectionable;</li>
					<li>Conduct yourself in an inappropriate, offensive, indecent, or vulgar manner while using our service or Site;</li>
					<li>Use the Site for any unlawful purpose;</li>
					<li>Upload, post, email, or otherwise transmit any content that you do not have a right to transmit under any law or under contractual or fiduciary relationships (such as inside information, proprietary, and confidential information learned or disclosed as part of employment relationships or under non-disclosure agreements);</li>
					<li>Upload, post, email, or otherwise transmit any content that infringes any patent, trademark, trade secret, copyright, or other intellectual property right of any party;</li>
					<li>Upload, post, email, or otherwise transmit any unsolicited or unauthorized advertising, promotional materials, “junk mail,” “spam,” “chain letters,” “pyramid schemes,” or any other form of solicitation;</li>
					<li>Upload, post, email, or otherwise transmit any material that contains software viruses or any other computer code, files, or programs designed to interrupt, destroy, or limit the functionality of any computer software or hardware or telecommunications equipment;</li>
					<li>Interfere with or disrupt the Site, the services, the content or servers or networks connected to the Site, the services or the content, or disobey any requirements, procedures, policies, or regulations of networks connected to the Site, the services, and/or the content, the terms of which are incorporated herein;</li>
					<p>Intentionally or unintentionally violate any applicable local, state, national, or international law.</p>

				</ul>
				<h2>Termination</h2>
				<p>Phen375 reserves the right to terminate your access to the Site or any of its services if it determines that you do not comply with these Terms of Use; provide false, inaccurate, or incomplete information during our registration process; engage in any conduct that would otherwise harm any of Phen375’s rights or interests in its Site, services, or other property; or for any or no reason whatsoever without prior notice to you. Upon termination, you must cease use of the Phen375 Site and destroy all materials obtained from such site and all copies thereof, whether made under the terms of these Terms of Use or otherwise.</p>
				<h2>User Submissions</h2>
				<p>Phen375 is free to use, without limitation, any comments, information, suggestions, messages, ideas, concepts, reviews, images or techniques contained in any communication you may send to the Site without any compensation, acknowledgement, or payment to you for any purpose whatsoever including, but not limited to, manufacturing, developing, marketing, and selling products and services, and creating, modifying or improving the Site or other websites. Furthermore, by posting any information on our Site, you grant us a non-exclusive, royalty-free, worldwide, perpetual license to display, use, reproduce or modify that information. Any information submitted on the Site is subject to our Privacy Statement, the terms of which are incorporated herein.</p>
				<h2>Disclaimer of Warranty</h2>
				<p>THE MATERIALS CONTAINED ON THE SITE ARE PROVIDED “AS IS” AND WITHOUT WARRANTIES OF ANY KIND EITHER EXPRESS OR IMPLIED. TO THE FULLEST EXTENT PERMISSIBLE PURSUANT TO APPLICABLE LAW, WE DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR OTHER VIOLATIONS OF RIGHTS. WE ASSUME NO LIABILITY OR RESPONSIBILITY FOR ANY ERRORS OR OMISSIONS IN THE CONTENT OF THE SITE, THAT DEFECTS WILL BE CORRECTED, OR THAT ANY PHEN375 SITE OR THE SERVERS THAT MAKE SUCH MATERIALS AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS; ANY FAILURES, DELAYS, MALFUNCTIONS, OR INTERRUPTIONS IN THE DELIVERY OF ANY CONTENT CONTAINED ON THE SITE; ANY LOSSES OR DAMAGES ARISING FROM THE USE OF THE CONTENT PROVIDED ON THE SITES; OR ANY CONDUCT BY USERS OF THE SITE, EITHER ONLINE OR OFFLINE. WE DO NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING THE USE OR THE RESULTS OF THE USE OF THE MATERIALS ON ANY PHEN375 SITE IN TERMS OF THEIR CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE. YOU ASSUME THE ENTIRE COST OF ALL NECESSARY SERVICING, REPAIR, OR CORRECTION.</p>
				<h2>Limitation of Liability</h2>
				<p>UNDER NO CIRCUMSTANCES, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE, SHALL PHEN375, ITS OFFICERS, DIRECTORS, EMPLOYEES, OR AGENTS BE LIABLE (JOINTLY OR SEVERALLY) FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND, INCLUDING, BUT NOT LIMITED TO, LOSS OF USE, DATA, OR PROFIT, ON ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE USE OR THE INABILITY TO USE THE MATERIALS ON THE SITE, EVEN IF PHEN375 OR ANY PHEN375 REPRESENTATIVE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES SO THE ABOVE LIMITATION OR EXCLUSION MAY NOT APPLY TO YOU. IF ANY APPLICABLE AUTHORITY HOLDS ANY PORTION OF THIS SECTION TO BE UNENFORCEABLE, THEN LIABILITY WILL BE LIMITED TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW.</p>
				<h2>Links</h2>
				<p>The Site may provide links to other World Wide Web sites or resources. Phen375 has not reviewed these sites and is not responsible for the accuracy, content, privacy policies or availability of information found on sites that link to or from any Phen375 Site, Phen375 uses affiliate networks who provide traffic and are responsible for maintaining all legal EU and USA marketing regulation compliance. Phen375 does not have any direct affiliates any off site material read is not under the jurisdiction of Phen375 or owning company Shippitsa ltd. We cannot ensure that you will be satisfied with any products or services that you purchase from a third-party site that links to or from any Phen375 Sites or third-party content on our sites. We do not endorse any of the merchandise, nor have we taken any steps to confirm the accuracy or reliability of any of the information contained in such third-party sites or content. We do not make any representations or warranties as to the security of any information (including, without limitation, credit card and other personal information) you might be requested to give any third party, and you hereby irrevocably waive any claim against Phen375 with respect to such sites and third-party content. We strongly encourage you to make whatever investigation you feel necessary or appropriate before proceeding with any online or offline transaction with any of these third parties. Neither Phen375 nor its, employees, directors, officers, or agents shall be liable for any damages, including but not limited to direct, indirect, incidental, consequential, or punitive damages arising out of your use of third-party material or third-party sites that are linked to this Site. No link to the Site may be framed to the extent that such frame contains any sponsorship, advertising, or other commercial text or graphics. All links to the Site must be to Phen375 . Deep linking to internal pages of this Site is expressly prohibited without prior written consent from Phen375 or Shippitsa ltd.</p>
				<h2>Indemnity</h2>
				<p>You agree to defend, indemnify, and hold harmless Shipitsa LTD, its officers, directors, employees, and agents, from and (i) against any claims, actions, or demands, including, but not limited to, reasonable legal and accounting fees, alleging or resulting from your use of the Site or (ii) your breach of these Terms of Use or (iii) your infringement of any intellectual property or privacy right of any person. Shipitsa LTD, shall provide notice to you promptly of any such claim, suit, or proceeding and shall assist you, at your expense, in defending any such claim, suit, or proceeding.</p>
				<h2>Choice of Law</h2>
				<p>You agree that any issue or dispute arising out of or in connection with your use of our Site, intellectual property, the Terms of Use, the Privacy Statement, or any matter concerning Shipitsa LTD shall be governed by the laws of the European Union , Scotland , Coatbridge. You agree that any such issue or dispute shall be brought exclusively in the federal or state courts located in the European Union , Scotland , Coatbridge Scottish Courts and Tribunals European Protection division . If any provision of the Terms of Use and Privacy Statement is found to be invalid by any court having competent jurisdiction, the invalidity of such provision shall not affect the validity of the remaining provisions of the Terms of Use and Privacy Statement, which shall remain in full force and effect.</p><p>
				By using the Site, you agree to comply with all applicable laws and regulations of the United States. The material provided on the Site is protected by law including, but not limited to, United States and European Union copyright and trademark law and international treaties. Phen375 makes no representation that materials contained in the Sited are appropriate or available for use in other locations and access to them from territories where their contents are illegal is prohibited. Those who choose to access the Site from other locations outside the United States or European Union do so at their own initiative and are responsible for compliance with applicable local laws.</p>

				<p>(EU office) Shippitsa LTD - Fountain Business Center Ellis Street, COATBRIDGE, Scotland, ML5 3AA . Upon purchasing from this site (the "Service"), you become a User and agree to be bound by this Agreement ("Return policy, Terms & Conditions"). Please read this Agreement carefully. By using the Service, you accept this Agreement and any modifications that may be made to the Agreement from time to time. If you do not agree to any provision of this Agreement, you should not use the Site or the Service</p>

				<p>Copyright and Trademark Notices All contents of the Site are ©2018. All rights reserved.</p>


               </div></div>

               <!-- /.feature-text -->
            </div>
            <hr>
            <div class="row-new2 row-none"> <div class="row last-banner">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <img src="http://www.maxslim.store/wp-content/uploads/2018/05/blot.png" class="img-responsive center-block" alt="Banner Image">
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h2 class="blue font40">
                    <span class="special-font extra-bold block-span">START YOUR JOURNEY TODAY! TRY PHEN375!</span>
                </h2>
                <h3 class="bold">No Prescription Required!</h3>
                <a href="http://www.maxslim.store/shop" class="button button-links">Rush My Order</a>
                <div style="margin-top: 30px;">
                    <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                        <img name="trustseal" alt="Security Seals" style="border: 0; width: 120px;" src="<?php echo get_theme_file_uri(); ?>/assets/def/img/6121-lg.gif" />
                    </a>
                </div>

                <span class="block-span">
                                      </span>
            </div>
           <!--  <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padd">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/fad.png" class="img-responsive seal-img" alt="Made in a FDA Approved Facility">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in a FDA</span>
                        <span class="dark-blue block-span special-font bold">Approved Facility</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img2.png" class="img-responsive seal-img" alt="100% Quality Guaranteed">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">100% Quality</span>
                        <span class="dark-blue block-span special-font bold">Guaranteed</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img3.png" class="img-responsive seal-img" alt="Made in USA">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in</span>
                        <span class="dark-blue block-span special-font bold">USA</span>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4 col-xs-12">
            <div class="row">
                <!-- <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-6 seals">
                                            <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
                            <tr>
                                <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
                                </td>
                            </tr>
                        </table>
                                    </div> -->

                <!-- <div class="col-md-12 col-sm-12 col-xs-1 seals">
                    <center>
                        <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                            <img name="trustseal" alt="Security Seals" style="border: 0; width: 160px;" src="//dw26xg4lubooo.cloudfront.net/seals/stacked/6121-lg.gif" />
                        </a>
                    </center>
                </div> -->
            </div>
        </div>
</div></div>
            <div class="spacer"></div>
         </div>
      </div>
						<!-- End Content -->

<?php get_footer( 'home' ); ?>