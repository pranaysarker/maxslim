<?php /* Template Name: Testimonials-guide */ 
	get_header( 'home' );
?>
								
								<div class="container">
										<div class="company-logo-top">
												<img src="../../dc411ibrlpprl.cloudfront.net/company-logo-top.jpg" alt="Matrix" id="tag" />										</div>

										
										<div class="visible-sm-12 menu-phone top-link">
												<div class="col-sm-12">
														<span><a href="track-my-order.html">Track My Order</a></span> |
														<span><a href="contact-us.html">Contact Us</a></span>
												</div>
												<div class="clearfix center-block select-container">
														<div class="pull-left">
																																<form  method="post">
																		<select id="currchangePhone" name="ChangeCurrency" onchange="this.form.submit()">
																				<option value="USD" selected='selected'>USD</option>
																				<option value="EUR" >EUR</option>
																				<option value="GBP" >GBP</option>
																				<option value="AUD" >AUD</option>
																				<option value="MXN" >MXN</option>
																				<option value="CAD" >CAD</option>
																		</select>
																</form>
														</div>
														<div class="pull-left">
																<select id="changLangPhone">
																		<option value='https://www.phen375.com/en/testimonials-guide.html' selected='selected'>English</option>
																		<option value='https://www.phen375.com/fr/testimonials-guide.html' >Francais</option>
																<!--Nestrix | Ing. Franco Salas-->
																		<!--Set Deutsch Language-->
																		<option value='https://www.phen375.com/de/testimonials-guide.html' >Deutsch</option>
																		<!--Set Greek Language-->
																		<option value='https://www.phen375.com/el/testimonials-guide.html' >Ελληνική</option>
																		<!--Set Spanish Language-->
																		<option value='https://www.phen375.com/es/testimonials-guide.html' >Español</option>
																		<!--Set Italian Language-->
																		<option value='https://www.phen375.com/it/testimonials-guide.html' >Italiano</option>
																		<!--Set Dutch Language-->
																		<option value='https://www.phen375.com/nl/testimonials-guide.html' >Dutch</option>
																<!--End Modifications-->
																</select>
														</div>
												</div>
										</div>

										<div class="containerMenuMobile overContentRelative">
											
																						<div id="menuMobile" class="overContent"></div>

											<!-- <div class="fullWidth fullHeight overContentRelative">
												<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
												</div>
											</div> -->
											<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
																									<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
														<tr>
															<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
															</td>
														</tr>
													</table>
																							</div>
											<a href="http://www.maxslim.store/shop" class="button inlineBlock overContent overContent-topRight containerMenuMobile-btnOrderNow">Order Now</a>										</div>

										
								</div>
						</div>
						<!-- End Header -->

						<!-- Content -->
						
						<link rel="canonical" href="testimonials-guide.html"/>
<div class="container" id="testimonial-guide">
	<div class="col-md-12 col-sm-12 col-xs-12 bor-bot-20 first-sec">
		<h1 class="blue text-right">
			<span class="bold span-block text-center">Share Your Experience</span> and Get Rewarded!
		</h1>

		<p class="text-center">	We value you as a client and your experience matters to us! We would appreciate your
			feedback and would like to use your positive experience to show others the effectiveness
			of Phen375. We appreciate your assistance with this effort, and would like to
			<span class="bold">Reward You for Sending Us Your Testimonial.</span></p>

		<div class="pos-rel">
			<img src="../../dc411ibrlpprl.cloudfront.net/newphen375/phen375-video-example.png" alt="Phen375 video example image" class="img-responsive center-block">

			<img src="../../dc411ibrlpprl.cloudfront.net/phen375/images/6_bottle_free.png"  alt="Phen375 video reward" class="pos-abs video-reward" width="width: 340px; !important" />
		</div>
	</div>

	<div class="col-md-12 col-sm-12 col-xs-12 blue-bk white send-video">
		<div class="col-md-8 col-sm-8 col-xs-12">
			<span>There are 2 types of Testimonials you can send us:</span>
			<h2>Send us a Video Testimonial and get a 3-Month Supply of Phen375!</h2>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<a href="send-testimonial.html" class="spcl-btn-green">Send Video</a>		</div>
	</div>

	<div class="col-md-12 col-sm-12 col-xs-12 picture-sample">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<img src="../../dc411ibrlpprl.cloudfront.net/phen375/images/4_bottle_free.jpg" alt="Phen375 picture reward" class="img-responsive center-block picture-reward">
			<p><span>Before and After Pictures</span> and we will Reward you with a 2-Month Supply of Phen375!</p>
			<a href="send-testimonial.html" class="spcl-btn-green">Send Pictures</a>		</div>
		<div class="col-md-6 col-sm-6 col-xs-12 pic-sample">
			<img src="../../dc411ibrlpprl.cloudfront.net/newphen375/picture-sample-phen375.jpg" alt="Phen375 picture example image" class="img-responsive center-block">
		</div>
	</div>

	<div class="col-md-12 col-sm-12 col-xs-12 blue-bk white">
		<h2 class="blue-banner">For Pictures, Stick to The Following Examples:</h2>
	</div>

	<div class="col-md-12 col-sm-12 col-xs-12 no-padd">
		<div class="col-md-4 col-sm-4 col-xs-12 no-padd">
			<img src="../../dc411ibrlpprl.cloudfront.net/newphen375/example_1.jpg" alt="Phen375 - example picture" class="img-responsive center-block">
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 no-padd">
			<img src="../../dc411ibrlpprl.cloudfront.net/newphen375/example_2.jpg" alt="Phen375 - example picture" class="img-responsive center-block">
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 no-padd">
			<img src="../../dc411ibrlpprl.cloudfront.net/newphen375/example_3.jpg" alt="Phen375 - example picture" class="img-responsive center-block">
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 no-padd">
			<img src="../../dc411ibrlpprl.cloudfront.net/newphen375/example_4.jpg" alt="Phen375 - example picture" class="img-responsive center-block">
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 no-padd">
			<img src="../../dc411ibrlpprl.cloudfront.net/newphen375/example_5.jpg" alt="Phen375 - example picture" class="img-responsive center-block">
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 no-padd">
			<img src="../../dc411ibrlpprl.cloudfront.net/newphen375/example_6.jpg" alt="Phen375 - example picture" class="img-responsive center-block">
		</div>
	</div>

	<div class="col-md-12 col-sm-12 col-xs-12 grey-bk">
		<div class="col-md-12 col-sm-12 col-xs-12 no-padd">
			<p class="text-center">	Make sure you send us a picture of your physical appearance before taking Phen375 and an after picture
				<span class="bold">holding your Phen375 bottle.</span> Usually, selfies do not come out as we desire so
				have someone take the pictures for you!</p>

			<h3 class="text-center bold">In Your Testimonial, Make Sure to Include:</h3>
		</div>

		<div class="col-md-3 col-sm-3 col-xs-12 no-padd icons">
			<img src="../../dc411ibrlpprl.cloudfront.net/newphen375/phen375-icns-1.png" alt="Phen375 - icon" class="img-responsive center-block">
			<span class="block-span text-center">Your starting Weight</span>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12 no-padd icons">
			<img src="../../dc411ibrlpprl.cloudfront.net/newphen375/phen375-icns-2.png" alt="Phen375 - icon" class="img-responsive center-block">
			<span class="block-span text-center">The weight loss you have accomplished</span>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12 no-padd icons">
			<img src="../../dc411ibrlpprl.cloudfront.net/newphen375/phen375-icns-3.png" alt="Phen375 - icon" class="img-responsive center-block">
			<span class="block-span text-center">Your weight after taking Phen375</span>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12 no-padd icons">
			<img src="../../dc411ibrlpprl.cloudfront.net/newphen375/phen375-icns-4.png" alt="Phen375 - icon" class="img-responsive center-block">
			<span class="block-span text-center no-padd">How your experience was while taking Phen375</span>
		</div>
	</div>

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="col-md-5 col-sm-5 col-xs-12">
			<!-- <img src="https://dc411ibrlpprl.cloudfront.net/phen375/en/botella1.jpg"
			     alt="" class="img-responsive center-block"> -->
			<img src="../../dc411ibrlpprl.cloudfront.net/newphen375/phen375-reward-img.jpg"
			     alt="" class="img-responsive center-block">
		</div>
		<div class="col-md-7 col-sm-7 col-xs-12 reward">
			<h3 class="bold">REWARD!</h3>
			<p>In order to send you your reward, we will need your name and address filled out in this form.
				In case you want your identity protected, just let us know on the Testimonial Identity Protection
				Field found in the Testimonial Form.</p>

			<a href="send-testimonial.html" class="spcl-btn-green">Send Testimonial</a>		</div>
	</div>

	<div class="col-md-12 col-sm-12 col-xs-12 disclaimer">
		<h2 class="blue bold">Testimonial Disclaimer</h2>

		<p>	All testimonials shown are real men and women. They may not reflect the typical user's product
			experience and are not intended to guarantee that anyone will achieve the same or similar results.
			Every person's lifestyle and habits have direct influence on results. These results are meant to
			showcase the achievements of some of the best and most motivated Phen375 users. Customers who provided
			Phen375 their success stories were remunerated with free product. Phen375 requires you to follow an
			eating plan and start an exercise program at times. Consult your physician before beginning any exercise
			or diet program or if you have pre-existing health conditions that may negatively interact with Phen375's
			ingredients.</p>
	</div>

</div>						<!-- End Content -->

<?php get_footer( 'home' ); ?>