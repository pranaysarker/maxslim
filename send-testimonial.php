<?php /* Template Name: send-testimonial */ 
	get_header( 'home' );
?>
								
								<div class="container">
										<div class="company-logo-top">
												<img src="../../dc411ibrlpprl.cloudfront.net/company-logo-top.jpg" alt="Matrix" id="tag" />										</div>

										
										<div class="visible-sm-12 menu-phone top-link">
												<div class="col-sm-12">
														<span><a href="track-my-order.html">Track My Order</a></span> |
														<span><a href="contact-us.html">Contact Us</a></span>
												</div>
												<div class="clearfix center-block select-container">
														<div class="pull-left">
																																<form  method="post">
																		<select id="currchangePhone" name="ChangeCurrency" onchange="this.form.submit()">
																				<option value="USD" selected='selected'>USD</option>
																				<option value="EUR" >EUR</option>
																				<option value="GBP" >GBP</option>
																				<option value="AUD" >AUD</option>
																				<option value="MXN" >MXN</option>
																				<option value="CAD" >CAD</option>
																		</select>
																</form>
														</div>
														<div class="pull-left">
																<select id="changLangPhone">
																		<option value='https://www.phen375.com/en/send-testimonial.html' selected='selected'>English</option>
																		<option value='https://www.phen375.com/fr/send-testimonial.html' >Francais</option>
																<!--Nestrix | Ing. Franco Salas-->
																		<!--Set Deutsch Language-->
																		<option value='https://www.phen375.com/de/send-testimonial.html' >Deutsch</option>
																		<!--Set Greek Language-->
																		<option value='https://www.phen375.com/el/send-testimonial.html' >Ελληνική</option>
																		<!--Set Spanish Language-->
																		<option value='https://www.phen375.com/es/send-testimonial.html' >Español</option>
																		<!--Set Italian Language-->
																		<option value='https://www.phen375.com/it/send-testimonial.html' >Italiano</option>
																		<!--Set Dutch Language-->
																		<option value='https://www.phen375.com/nl/send-testimonial.html' >Dutch</option>
																<!--End Modifications-->
																</select>
														</div>
												</div>
										</div>

										<div class="containerMenuMobile overContentRelative">
											
																						<div id="menuMobile" class="overContent"></div>

											<!-- <div class="fullWidth fullHeight overContentRelative">
												<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
												</div>
											</div> -->
											<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
																									<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
														<tr>
															<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
															</td>
														</tr>
													</table>
																							</div>
											<a href="http://www.maxslim.store/shop" class="button inlineBlock overContent overContent-topRight containerMenuMobile-btnOrderNow">Order Now</a>										</div>

										
								</div>
						</div>
						<!-- End Header -->

						<!-- Content -->
						
						<link rel="canonical" href="send-testimonial.html" />
<script type='text/javascript'>
$.cloudinary.config({"api_key":"134759663375852","cloud_name":"sun-pyramid-health"});
</script>

<div class="container">
	<div class="col-md-12 col-sm-12 col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading">
				<h1 class="text-center">Send Your Testimonial</h1>
			</div>
			<div class="panel-body">
				<div class="well"><span class="badge">1</span>	Please provide your information</div>
								<div class='alert alert-danger alert-div disp-none'><p class="text-center bold"><span class="alert-msg block-span"></span></p></div>

				<form method="post" enctype="multipart/form-data" id="testimonioal-form" data-countrycode="BD">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="first-name">First Name</label>
							<input type="text" class="form-control" id="first-name" name="first-name" placeholder="Enter your first name" autofocus required>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="last-name">Last Name</label>
							<input type="text" class="form-control" id="last-name" name="last-name" placeholder="Enter your last name" required>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="email">Email address</label>
							<input type="email" class="form-control" id="email" name="email" placeholder="Enter your email address" required>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="form-group">
							<label for="phone">Phone Number</label>
							<input type="text" class="form-control" id="phone" name="phone" placeholder="Enter your phone number" required>
						</div>
					</div>
					<!--Shipping info-->
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="form-group">
							<label for="country">Country</label>
							<select name="country" id="country" class="form-control" required>
								<option value="AX">Aaland Islands</option>
								<option value="AL">Albania</option>
								<option value="DZ">Algeria</option>
								<option value="AS">American Samoa</option>
								<option value="AD">Andorra</option>
								<option value="AO">Angola</option>
								<option value="AI">Anguilla</option>
								<option value="AQ">Antarctica</option>
								<option value="AG">Antigua and Barbuda</option>
								<option value="AR">Argentina</option>
								<option value="AM">Armenia</option>
								<option value="AW">Aruba</option>
								<option value="AU">Australia</option>
								<option value="AT">Austria</option>
								<option value="AZ">Azerbaijan</option>
								<option value="BS">Bahamas</option>
								<option value="BH">Bahrain</option>
								<option value="BD">Bangladesh</option>
								<option value="BB">Barbados</option>
								<option value="BY">Belarus</option>
								<option value="BE">Belgium</option>
								<option value="BZ">Belize</option>
								<option value="BJ">Benin</option>
								<option value="BM">Bermuda</option>
								<option value="BT">Bhutan</option>
								<option value="BO">Bolivia</option>
								<option value="BA">Bosnia and Herzegowina</option>
								<option value="BW">Botswana</option>
								<option value="BV">Bouvet Island</option>
								<option value="BR">Brazil</option>
								<option value="IO">British Indian Ocean Territory</option>
								<option value="BN">Brunei Darussalam</option>
								<option value="BG">Bulgaria</option>
								<option value="BF">Burkina Faso</option>
								<option value="BI">Burundi</option>
								<option value="KH">Cambodia</option>
								<option value="CM">Cameroon</option>
								<option value="CA">Canada</option>
								<option value="CV">Cape Verde</option>
								<option value="KY">Cayman Islands</option>
								<option value="CF">Central African Republic</option>
								<option value="TD">Chad</option>
								<option value="CL">Chile</option>
								<option value="CN">China</option>
								<option value="CX">Christmas Island</option>
								<option value="CC">Cocos (Keeling) Islands</option>
								<option value="CO">Colombia</option>
								<option value="KM">Comoros</option>
								<option value="CG">Congo</option>
								<option value="CK">Cook Islands</option>
								<option value="CR">Costa Rica</option>
								<option value="CI">Cote D'Ivoire</option>
								<option value="HR">Croatia</option>
								<option value="CY">Cyprus</option>
								<option value="CZ">Czech Republic</option>
								<option value="DK">Denmark</option>
								<option value="DJ">Djibouti</option>
								<option value="DM">Dominica</option>
								<option value="DO">Dominican Republic</option>
								<option value="TP">East Timor</option>
								<option value="EC">Ecuador</option>
								<option value="EG">Egypt</option>
								<option value="SV">El Salvador</option>
								<option value="GQ">Equatorial Guinea</option>
								<option value="ER">Eritrea</option>
								<option value="EE">Estonia</option>
								<option value="ET">Ethiopia</option>
								<option value="FK">Falkland Islands (Malvinas)</option>
								<option value="FO">Faroe Islands</option>
								<option value="FJ">Fiji</option>
								<option value="FI">Finland</option>
								<option value="FR">France</option>
								<option value="FX">France, Metropolitan</option>
								<option value="GF">French Guiana</option>
								<option value="PF">French Polynesia</option>
								<option value="TF">French Southern Territories</option>
								<option value="GA">Gabon</option>
								<option value="GM">Gambia</option>
								<option value="GE">Georgia</option>
								<option value="DE">Germany</option>
								<option value="GI">Gibraltar</option>
								<option value="GR">Greece</option>
								<option value="GL">Greenland</option>
								<option value="GD">Grenada</option>
								<option value="GP">Guadeloupe</option>
								<option value="GU">Guam</option>
								<option value="GT">Guatemala</option>
								<option value="GG">Guernsey</option>
								<option value="GN">Guinea</option>
								<option value="GW">Guinea-bissau</option>
								<option value="GY">Guyana</option>
								<option value="HT">Haiti</option>
								<option value="HM">Heard and Mc Donald Islands</option>
								<option value="HN">Honduras</option>
								<option value="HK">Hong Kong</option>
								<option value="HU">Hungary</option>
								<option value="IS">Iceland</option>
								<option value="IN">India</option>
								<option value="ID">Indonesia</option>
								<option value="IE">Ireland</option>
								<option value="IM">Isle of Man</option>
								<option value="IL">Israel</option>
								<option value="IT">Italy</option>
								<option value="JM">Jamaica</option>
								<option value="JP">Japan</option>
								<option value="JY">Jersey</option>
								<option value="JO">Jordan</option>
								<option value="KZ">Kazakhstan</option>
								<option value="KE">Kenya</option>
								<option value="KI">Kiribati</option>
								<option value="KR">Korea, Republic of</option>
								<option value="KW">Kuwait</option>
								<option value="KG">Kyrgyzstan</option>
								<option value="LA">Lao People's Democratic Republic</option>
								<option value="LV">Latvia</option>
								<option value="LB">Lebanon</option>
								<option value="LS">Lesotho</option>
								<option value="LR">Liberia</option>
								<option value="LI">Liechtenstein</option>
								<option value="LT">Lithuania</option>
								<option value="LU">Luxembourg</option>
								<option value="MO">Macau</option>
								<option value="MK">Macedonia, The Former Yugoslav Republic of</option>
								<option value="MG">Madagascar</option>
								<option value="MW">Malawi</option>
								<option value="MY">Malaysia</option>
								<option value="MV">Maldives</option>
								<option value="ML">Mali</option>
								<option value="MT">Malta</option>
								<option value="MH">Marshall Islands</option>
								<option value="MQ">Martinique</option>
								<option value="MR">Mauritania</option>
								<option value="MU">Mauritius</option>
								<option value="YT">Mayotte</option>
								<option value="MX">Mexico</option>
								<option value="FM">Micronesia, Federated States of</option>
								<option value="MD">Moldova, Republic of</option>
								<option value="MC">Monaco</option>
								<option value="MN">Mongolia</option>
								<option value="MS">Montserrat</option>
								<option value="MA">Morocco</option>
								<option value="MZ">Mozambique</option>
								<option value="MM">Myanmar</option>
								<option value="NA">Namibia</option>
								<option value="NR">Nauru</option>
								<option value="NP">Nepal</option>
								<option value="NL">Netherlands</option>
								<option value="AN">Netherlands Antilles</option>
								<option value="NC">New Caledonia</option>
								<option value="NZ">New Zealand</option>
								<option value="NI">Nicaragua</option>
								<option value="NE">Niger</option>
								<option value="NU">Niue</option>
								<option value="NF">Norfolk Island</option>
								<option value="ND">Northern Ireland</option>
								<option value="MP">Northern Mariana Islands</option>
								<option value="NO">Norway</option>
								<option value="OM">Oman</option>
								<option value="PW">Palau</option>
								<option value="PA">Panama</option>
								<option value="PG">Papua New Guinea</option>
								<option value="PY">Paraguay</option>
								<option value="PE">Peru</option>
								<option value="PH">Philippines</option>
								<option value="PN">Pitcairn</option>
								<option value="PL">Poland</option>
								<option value="PT">Portugal</option>
								<option value="PR">Puerto Rico</option>
								<option value="QA">Qatar</option>
								<option value="RE">Reunion</option>
								<option value="RO">Romania</option>
								<option value="RU">Russian Federation</option>
								<option value="RW">Rwanda</option>
								<option value="KN">Saint Kitts and Nevis</option>
								<option value="LC">Saint Lucia</option>
								<option value="VC">Saint Vincent and the Grenadines</option>
								<option value="WS">Samoa</option>
								<option value="SM">San Marino</option>
								<option value="ST">Sao Tome and Principe</option>
								<option value="SA">Saudi Arabia</option>
								<option value="SS">Scotland</option>
								<option value="SN">Senegal</option>
								<option value="SC">Seychelles</option>
								<option value="SL">Sierra Leone</option>
								<option value="SG">Singapore</option>
								<option value="SK">Slovakia (Slovak Republic)</option>
								<option value="SI">Slovenia</option>
								<option value="SB">Solomon Islands</option>
								<option value="SO">Somalia</option>
								<option value="ZA">South Africa</option>
								<option value="GS">South Georgia and the South Sandwich Islands</option>
								<option value="ES">Spain</option>
								<option value="LK">Sri Lanka</option>
								<option value="SH">St. Helena</option>
								<option value="PM">St. Pierre and Miquelon</option>
								<option value="SD">Sudan</option>
								<option value="SR">Suriname</option>
								<option value="SJ">Svalbard and Jan Mayen Islands</option>
								<option value="SZ">Swaziland</option>
								<option value="SE">Sweden</option>
								<option value="CH">Switzerland</option>
								<option value="SY">Syrian Arab Republic</option>
								<option value="TW">Taiwan</option>
								<option value="TJ">Tajikistan</option>
								<option value="TZ">Tanzania, United Republic of</option>
								<option value="TH">Thailand</option>
								<option value="TG">Togo</option>
								<option value="TK">Tokelau</option>
								<option value="TO">Tonga</option>
								<option value="TT">Trinidad and Tobago</option>
								<option value="TN">Tunisia</option>
								<option value="TR">Turkey</option>
								<option value="TM">Turkmenistan</option>
								<option value="TC">Turks and Caicos Islands</option>
								<option value="TV">Tuvalu</option>
								<option value="UG">Uganda</option>
								<option value="UA">Ukraine</option>
								<option value="AE">United Arab Emirates</option>
								<option value="GB">United Kingdom</option>
								<option value="US">United States</option>
								<option value="UM">United States Minor Outlying Islands</option>
								<option value="UY">Uruguay</option>
								<option value="UZ">Uzbekistan</option>
								<option value="VU">Vanuatu</option>
								<option value="VA">Vatican City State (Holy See)</option>
								<option value="VE">Venezuela</option>
								<option value="VN">Viet Nam</option>
								<option value="VG">Virgin Islands (British)</option>
								<option value="VI">Virgin Islands (U.S.)</option>
								<option value="WL">Wales</option>
								<option value="WF">Wallis and Futuna Islands</option>
								<option value="EH">Western Sahara</option>
								<option value="YE">Yemen</option>
								<option value="YU">Yugoslavia</option>
								<option value="ZR">Zaire</option>
								<option value="ZM">Zambia</option>
							</select>
						</div>
						<input type="hidden" name="country-name" id="countryName">
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="form-group">
							<label for="state" id="stateLabel">State</label>
							<select name="state" id="state" required></select>
						</div>
						<input type="hidden" name="state-name" id="stateName">
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="form-group">
							<label for="city">City</label>
							<input type="text" class="form-control" id="city" name="city" placeholder="Enter city" required>
						</div>
					</div>
					<div class="col-md-8 col-sm-8 col-xs-12">
						<div class="form-group">
							<label for="address">Full Address</label>
							<input type="text" class="form-control" id="address" name="address" placeholder="Enter your full address" required>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="form-group">
							<label for="zip-code">ZIP Code</label>
							<input type="text" class="form-control" id="zip-code" name="zip-code" placeholder="Enter your ZIP code" required>
						</div>
					</div>
					<!--End Shipping Info-->
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<label for="testimony">Tell us about your experience using Phen375</label>
							<textarea id="testimony" name="testimony" cols="30" rows="10" class="form-control" required></textarea>
						</div>
						<hr>
						<div class="well"><span class="badge">2</span>	Please upload your pictures</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class='alert alert-danger error-div disp-none'><p class="text-center">Something wrong happened. <span class="error-msg block-span"></span> Please try again later.</p></div>

						<div class="col-md-6 col-sm-6 col-xs-12 mb-30">
							<div class="form-group mt-50" id="beforePic">
								<label for="beforePicture" class="block-span text-center">Upload Your Before Phen375 Picture</label>
								<p class="text-center">
									<span class="btn btn-info btn-file">
										<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> Chose File
										<input accept='image/gif, image/jpeg, image/png' class='upload-btn cloudinary-fileupload' data-cloudinary-field='phen375_testimonial' data-dropzone='beforePic' data-form-data='{"timestamp":1526386417,"allowed_formats":"png,jpeg,jpg,gif","callback":"https:\/\/www.phen375.com:443\/cloudinary_cors.html","overwrite":"1","public_id":"BF-277d8d1a","tags":"phen375,beforePhen375_Picture_2018-05-15,2018-05-15,277d8d1a","signature":"36d2f8889d8b5c8633fda575a3842dd90ddb5162","api_key":"134759663375852"}' data-url='https://api.cloudinary.com/v1_1/sun-pyramid-health/image/upload' name='file' type='file'/>									</span>
								</p>
								<p class="help-block text-center">(Limit 2 Mb)</p>

								<!-- status box -->
								<div class="progress disp-none">
									<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
										<span class="percentage">0%</span> Complete
									</div>
								</div>

								<div class="uploaded-info-container"></div>
								<input type="hidden" class="url-picture" name="before-phen375-picture" id="before-phen375-picture">
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 mb-30">
							<div class="form-group mt-50" id="afterPic">
								<label for="afterPicture" class="block-span text-center">Upload Your After Phen375 Picture</label>
								<p class="text-center">
									<span class="btn btn-info btn-file">
										<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> Chose File
										<input accept='image/gif, image/jpeg, image/png' class='upload-btn cloudinary-fileupload' data-cloudinary-field='phen375_testimonial' data-dropzone='afterPic' data-form-data='{"timestamp":1526386417,"allowed_formats":"png,jpeg,jpg,gif","callback":"https:\/\/www.phen375.com:443\/cloudinary_cors.html","overwrite":"1","public_id":"AF-277d8d1a","tags":"phen375,AfterPhen375_Picture_2018-05-15,2018-05-15,277d8d1a","signature":"1ccfd2345b5c230390fb277bf97b2bedbb75079c","api_key":"134759663375852"}' data-url='https://api.cloudinary.com/v1_1/sun-pyramid-health/image/upload' name='file' type='file'/>									</span>
								</p>
								<p class="help-block text-center">(Limit 2 Mb)</p>

								<!-- status box -->
								<div class="progress disp-none">
									<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
										<span class="percentage">0%</span> Complete
									</div>
								</div>

								<div class="uploaded-info-container"></div>
								<input type="hidden" class="url-picture" name="after-phen375-picture" id="after-phen375-picture">
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 mb-30">
							<hr>
							<div class="form-group mt-50" id="video-phen375">
								<label for="afterPicture" class="block-span text-center">Upload Your Video</label>
								<p class="text-center">
									<span class="btn btn-warning btn-file">
										<span class="glyphicon glyphicon-film" aria-hidden="true"></span> Chose File
										<input class='upload-video-btn cloudinary-fileupload' data-cloudinary-field='test' data-dropzone='video-phen375' data-form-data='{"timestamp":1526386417,"allowed_formats":"mp4,webm,flv,mov,ogv,3gp,3g2,wmv,mpeg,flv,mkv,avi","callback":"https:\/\/www.phen375.com:443\/cloudinary_cors.html","overwrite":"1","public_id":"Video-277d8d1a","tags":"phen375,Video_Phen375_2018-05-15,2018-05-15","signature":"28a972e82f0be912c9e266de92dfb54050f21917","api_key":"134759663375852"}' data-max-chunk-size='6_000_000' data-url='https://api.cloudinary.com/v1_1/sun-pyramid-health/video/upload_chunked' name='file' type='file'/>									</span>
								</p>
								<p class="help-block text-center">(Limit 40 Mb)</p>

								<!-- status box -->
								<div class="progress disp-none">
									<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
										<span class="percentage">0%</span> Complete
									</div>
								</div>

								<div class="uploaded-info-container"></div>
								<input type="hidden" class="url-video" name="phen375-video" id="phen375-video">
							</div>
						</div>

					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="well"><span class="badge">3</span>	Submit your information to complete your testimonial</div>

						<div class="alert alert-warning text-center">
							<div class="form-group">
								<h3> Protect My Identity</h3>
								<p>
									<label class="radio-inline">
										<input type="radio" name="protect-identity" value="Yes"> Yes
									</label>
									<label class="radio-inline">
										<input type="radio" name="protect-identity" value="No" checked> No
									</label>
								</p>
							</div>

							<div class="form-group mt-50">
								<button type="submit" class="btn btn-primary btn-lg fancybx" id="submit">Submit <span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span></button>
							</div>
						</div>
					</div>
				</form>

			</div>
		</div>

	</div>
</div>

<script>
$(function() {
	/*Get States*/
	var $rootFolder = 'https://www.phen375.com/';
	var countryCode = $("#testimonioal-form").data('countrycode');

	var placeholder = 'Remember that in your Testimonial Text, you need to include: \n- Your starting weight \n- The weight loss accomplished \n- Your weight after taking Phen375 \n- How your experience was while taking Phen375';
	$('#testimony').attr('placeholder', placeholder);

	if(countryCode != null){
		$('#country').val(countryCode);
		$('#stateLabel').next().remove();
		$('#stateLabel').parent().append('<img src="https://dc411ibrlpprl.cloudfront.net/newphen375/ajax-loader.gif" style="float:right; margin-right:40%">');

		var request = $.ajax({
			url: $rootFolder+"conversions/getStatesForCountry/"+countryCode,
			type: "GET",
			scriptCharset: "UTF-8" ,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			dataType: "text"
		});
		request.done(function( msg ) {
			if(msg == 0) {
				$('#stateLabel').next().remove();
			}else{
				$('#stateLabel').next().remove();
				$('#stateLabel').parent().append( msg );
				$('#state').addClass('form-control');
			}
		});
		request.fail(function( jqXHR, textStatus ) {
			alert( "Request failed: " + textStatus );
		});
	}

	$("#country").change(function() {
		var countryCode = this.value;

		if(countryCode != null){
			$('#stateLabel').next().remove();
			$('#stateLabel').parent().append('<img src="https://dc411ibrlpprl.cloudfront.net/newphen375/ajax-loader.gif" style="float:right; margin-right:40%">');

			var request = $.ajax({
				url: $rootFolder+"conversions/getStatesForCountry/"+countryCode,
				type: "GET",
				scriptCharset: "UTF-8" ,
				contentType: "application/x-www-form-urlencoded; charset=UTF-8",
				dataType: "text"
			});
			request.done(function( msg ) {
				if(msg == 0) {
					$('#stateLabel').next().remove();
				}else{
					$('#stateLabel').next().remove();
					$('#stateLabel').parent().append( msg );
					$('#state').addClass('form-control');
				}
			});
			request.fail(function( jqXHR, textStatus ) {
				alert( "Request failed: " + textStatus );
			});
		}
	});

	/*Validate form fields*/
	var isEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var isPhone = /^(\d)?(\s|\+)?(\d)+(\s|\-)?(\d)+(\s|\-)?(\d)+$/;
    var isZipCode = /^([1-9\-])+$/;
    var isAlphanumeric = /^[\w\;\.\,\"\-\+\s\/\!\'\_]+$/;
    var info = 0;

	function showAlertMsg(message, fieldId) {
		$("html, body").animate({scrollTop: 0}, 400);
		$(".alert-msg").html(message);
		$(".alert-div").fadeIn();
		setTimeout(function(){
			$(".alert-div").fadeOut();
		},8000);
		$(fieldId).parents('div.form-group').addClass('has-error');
		$(fieldId).focus();
		return false;
	}

	function isEmpty(fieldId) {
		var value = $(fieldId).val();
		return value == '';
	}

    $('#first-name').on('focusout', function() {
		if (isEmpty('#first-name')) {
			showAlertMsg("Please enter a first name. This field is required.", "#first-name");
		} else if (!isAlphanumeric.test($('#first-name').val())) {
			showAlertMsg("Please enter correct first name. Do not use special characters.", "#first-name");
		} else {
			$('#first-name').parents('div.form-group').removeClass('has-error');
		}
    });

    $('#last-name').on('focusout', function() {
		if (isEmpty('#last-name')) {
			showAlertMsg("Please enter a last name. This field is required.", "#last-name");
		} else if (!isAlphanumeric.test($('#last-name').val())) {
			showAlertMsg("Please enter correct last name. Do not use special characters.", "#last-name");
		} else {
			$('#last-name').parents('div.form-group').removeClass('has-error');
		}
    });

    $('#email').on('focusout', function() {
		if (isEmpty('#email')) {
			showAlertMsg("Please enter an email address. This field is required.", "#email");
		} else if (!isEmail.test($('#email').val())) {
			showAlertMsg("Please enter a valid email address.", "#email");
		} else {
			$('#email').parents('div.form-group').removeClass('has-error');
		}
    });

    $('#phone').on('focusout', function() {
		if (isEmpty('#phone')) {
			showAlertMsg("Please enter a phone number. This field is required.", "#phone");
		} else if (!isPhone.test($('#phone').val())) {
			showAlertMsg("Please enter a valid phone number.", "#phone");
		} else {
			$('#phone').parents('div.form-group').removeClass('has-error');
		}
    });

    $('#city').on('focusout', function() {
		if (isEmpty('#city')) {
			showAlertMsg("Please enter city. This field is required.", "#city");
		} else if (!isAlphanumeric.test($('#city').val())) {
			showAlertMsg("Please do not insert special characters on city.", "#city");
		} else {
			$('#city').parents('div.form-group').removeClass('has-error');
		}
    });

    $('#address').on('focusout', function() {
		if (isEmpty('#address')) {
			showAlertMsg("Please enter your full address. This field is required.", "#address");
		} else if (!isAlphanumeric.test($('#address').val())) {
			showAlertMsg("Please do not insert special characters on address.", "#address");
		} else {
			$('#address').parents('div.form-group').removeClass('has-error');
		}
    });

    $('#zip-code').on('focusout', function() {
		if (isEmpty('#zip-code')) {
			showAlertMsg("Please enter a zip code. This field is required.", "#zip-code");
		} else if (!isPhone.test($('#zip-code').val())) {
			showAlertMsg("Please enter a valid zip code.", "#zip-code");
		} else {
			$('#zip-code').parents('div.form-group').removeClass('has-error');
		}
    });

    $('#testimony').on('focusout', function() {
		if (isEmpty('#testimony')) {
			showAlertMsg("Please enter your testimony. This field is required.", "#testimony");
		} else if (!isAlphanumeric.test($('#testimony').val())) {
			showAlertMsg("Please do not insert special characters on testimony.", "#testimony");
		} else {
			$('#testimony').parents('div.form-group').removeClass('has-error');
		}
    });

    $('#submit').on('click', function(e) {
		$countryName = $("#country option:selected").text();
		$('#countryName').val($countryName);
		$stateName = $("#state option:selected").text();
		$('#stateName').val($stateName);
    });

    $('#testimonioal-form').on('submit', function(e) {
		if (isEmpty("#before-phen375-picture") || isEmpty("#after-phen375-picture")) {
			e.preventDefault();
			$(".error-div").html("<p class='text-center'>Please upload before phen375 & after phen375 pictures!<p>");
			$(".error-div").fadeIn();
			setTimeout(function(){
				$(".error-div").fadeOut();
			}, 8000);
		}
	});

	/*Pictures*/
	$('.upload-btn').on('click', function(e){
		if ((isEmpty('#first-name')) || (isEmpty('#last-name')) || (isEmpty('#email')) || (isEmpty('#phone')) || (isEmpty('#city')) || (isEmpty('#address')) || (isEmpty('#zip-code')) || (isEmpty('#testimony'))) {
			e.preventDefault();
			$(".error-msg").html("Please fill all required information before upload your pictures");
			$(".error-div").fadeIn();
			setTimeout(function(){
				$(".error-div").fadeOut();
			}, 8000);
		}

		var dropZone = '#' + $(this).data('dropzone');
		var progressBar = dropZone + " .progress";
		var hideBtn = dropZone + " .btn, " + dropZone + " .help-block";
		var container = dropZone + " .uploaded-info-container";
		var progBar = dropZone + " .progress-bar";
		var percentage = dropZone + " .percentage";
		var uploadBtn = dropZone + " .cloudinary-fileupload";
		var url = dropZone + " .url-picture";

		$(uploadBtn).cloudinary_fileupload({
			dropZone: dropZone,
			start: function () {
				$(hideBtn).fadeOut();
			},
			progress: function (e, data) {
				$(progressBar).fadeIn();
				var progress = Math.round((data.loaded * 100.0) / data.total);
				$(progBar).css('width', progress + '%');
				$(percentage).html(progress + '%');
			},
		}).on('cloudinarydone', function (e, data) {
			$(progressBar).fadeOut();
			$(container).find('.msg').remove();
			$(container).append(
				$.cloudinary.image(data.result.public_id, {
					format: data.result.format, width: 150, height: 150, crop: "thumb", class: "img-responsive center-block msg"
				})
			);
			$(url).val(data.result.secure_url);
			$(container).append('<p class="msg text-center"><span class="label label-success">The picture was uploaded successfully!</span></p>');
			$(container).append('<p class="msg text-center"><a href="#" class="btn btn-danger change-img" data-zone="'+ dropZone +'">Delete</a></p>');
		}).on('fileuploadfail', function(e, data) {
			var error = data._response.jqXHR.responseJSON.error.message;
			console.log(error);
			if(typeof(error) != "undefined" && error !== null) {
				$(".error-msg").html(error);
				$(".error-div").fadeIn();
				setTimeout(function(){
					$(".error-div").fadeOut();
				}, 8000);
				$(progressBar).fadeOut();
				$(hideBtn).fadeIn();
			} else {
				$(".error-div").fadeIn();
				setTimeout(function(){
					$(".error-div").fadeOut();
				}, 8000);
				$(progressBar).fadeOut();
				$(hideBtn).fadeIn();
			}
		});
	});

	/*Delete Button*/
	$(document).on("click", ".change-img", function(e){
		e.preventDefault();
		var zone = $(this).data('zone');
		var showBtn = zone + " .btn, " + zone + " .help-block";
		var container = zone + " .uploaded-info-container";

		$(container).find('.msg').remove();
		$(showBtn).fadeIn();
	});

	/*Video*/
	$('.upload-video-btn').on('click', function(e){
		if ((isEmpty('#first-name')) || (isEmpty('#last-name')) || (isEmpty('#email')) || (isEmpty('#phone')) || (isEmpty('#city')) || (isEmpty('#address')) || (isEmpty('#zip-code')) || (isEmpty('#testimony'))) {
			e.preventDefault();
			$(".error-msg").html("Please fill all required information before upload your video");
			$(".error-div").fadeIn();
			setTimeout(function(){
				$(".error-div").fadeOut();
			}, 8000);
		}

		var dropZone = '#' + $(this).data('dropzone');
		var progressBar = dropZone + " .progress";
		var hideBtn = dropZone + " .btn, " + dropZone + " .help-block";
		var container = dropZone + " .uploaded-info-container";
		var progBar = dropZone + " .progress-bar";
		var percentage = dropZone + " .percentage";
		var uploadBtn = dropZone + " .cloudinary-fileupload";
		var url = dropZone + " .url-video";

		$(uploadBtn).cloudinary_fileupload({
			dropZone: dropZone,
			maxFileSize: 5000000,
			start: function () {
				$(hideBtn).fadeOut();
			},
			progress: function (e, data) {
				$(progressBar).fadeIn();
				var progress = Math.round((data.loaded * 100.0) / data.total);
				$(progBar).css('width', progress + '%');
				$(percentage).html(progress + '%');
			}
		}).on('cloudinarydone', function (e, data) {
			$(progressBar).fadeOut();
			$(container).find('.msg').remove();
			$(container).append(
				$.cloudinary.image(data.result.public_id, {
					format: "jpg", width: 150, height: 150, crop: "fill", class: "img-responsive center-block msg", resource_type: "video"
				})
			);
			$(url).val(data.result.secure_url);
			$(container).append('<p class="msg text-center"><span class="label label-success">The video was uploaded successfully!</span></p>');
			$(container).append('<p class="msg text-center"><a href="#" class="btn btn-danger change-img" data-zone="'+ dropZone +'">Delete</a></p>');
		}).on('fileuploadfail', function(e, data) {
			var error = data._response.jqXHR.responseJSON.error.message;
			console.log(error);
			if(typeof(error) != "undefined" && error !== null) {
				$(".error-msg").html(error);
				$(".error-div").fadeIn();
				setTimeout(function(){
					$(".error-div").fadeOut();
				}, 8000);
				$(progressBar).fadeOut();
				$(hideBtn).fadeIn();
			} else {
				$(".error-div").fadeIn();
				$(progressBar).fadeOut();
				$(hideBtn).fadeIn();
			}
		});
	});
});

</script>						<!-- End Content -->

<?php get_footer( 'home' ); ?>