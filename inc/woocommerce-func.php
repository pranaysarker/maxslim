<?php 
add_action( 'after_setup_theme', 'woocommerce_maxslim_func');
function woocommerce_maxslim_func(){
	// price
	function the_dramatist_price_show() {
	    global $product;
	    if( $product->is_on_sale() ) {
	        return $product->get_sale_price();
	    }
	    return $product->get_regular_price();
	}

	// removing undwanted hooks
	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb',20 );
	
	remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
}

