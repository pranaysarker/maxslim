<?php /* Template Name: TrackMyOrder */ 
	get_header( 'home' );
?>
								
								<div class="container">
										<div class="company-logo-top">
												<img src="<?php echo get_theme_file_uri(); ?>/company-logo-top.jpg" alt="Matrix" id="tag" />										</div>

										
										<div class="visible-sm-12 menu-phone top-link">
												<div class="col-sm-12">
														<span><a href="track-my-order.html">Track My Order</a></span> |
														<span><a href="contact-us.html">Contact Us</a></span>
												</div>
												<div class="clearfix center-block select-container">
														<div class="pull-left">
																																<form  method="post">
																		<select id="currchangePhone" name="ChangeCurrency" onchange="this.form.submit()">
																				<option value="USD" selected='selected'>USD</option>
																				<option value="EUR" >EUR</option>
																				<option value="GBP" >GBP</option>
																				<option value="AUD" >AUD</option>
																				<option value="MXN" >MXN</option>
																				<option value="CAD" >CAD</option>
																		</select>
																</form>
														</div>
														<div class="pull-left">
																<select id="changLangPhone">
																		<option value='https://www.phen375.com/en/track-my-order.html' selected='selected'>English</option>
																		<option value='https://www.phen375.com/fr/track-my-order.html' >Francais</option>
																<!--Nestrix | Ing. Franco Salas-->
																		<!--Set Deutsch Language-->
																		<option value='https://www.phen375.com/de/track-my-order.html' >Deutsch</option>
																		<!--Set Greek Language-->
																		<option value='https://www.phen375.com/el/track-my-order.html' >Ελληνική</option>
																		<!--Set Spanish Language-->
																		<option value='https://www.phen375.com/es/track-my-order.html' >Español</option>
																		<!--Set Italian Language-->
																		<option value='https://www.phen375.com/it/track-my-order.html' >Italiano</option>
																		<!--Set Dutch Language-->
																		<option value='https://www.phen375.com/nl/track-my-order.html' >Dutch</option>
																<!--End Modifications-->
																</select>
														</div>
												</div>
										</div>

										<div class="containerMenuMobile overContentRelative">
											
																						<div id="menuMobile" class="overContent"></div>

											<!-- <div class="fullWidth fullHeight overContentRelative">
												<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
												</div>
											</div> -->
											<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
																									<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
														<tr>
															<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
															</td>
														</tr>
													</table>
																							</div>
											<a href="http://www.maxslim.store/shop" class="button inlineBlock overContent overContent-topRight containerMenuMobile-btnOrderNow">Order Now</a>										</div>

										
								</div>
						</div>
						<!-- End Header -->

						<!-- Content -->
						
						<link rel="canonical" href="track-my-order.html" />
<div class="container">
	<div class="row">
		<div class="col-md-12 block">
			<h1>Track your order:</h1>
			<p>Please insert the same email address that you used to place your order.</p>
			<p>Example: youremail@domain.com</p>
			<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
				<div class="panel panel-primary">
					<!-- Default panel contents -->
					<div class="panel-heading">
						<h2 class="main-title">Package Status</h2>
					</div>
					<div class="panel-body">
						<form class="form-horizontal">
							<div class="form-group">
								<label for="email" class="col-sm-2 control-label">Email</label>
								<div class="col-sm-9">
									<input type="email" class="form-control" id="email" placeholder="youremail@domain.com" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button id="search" type="button" data-loading-text="Loading..." class="btn btn-primary" autocomplete="off">Submit</button>
								</div>
							</div>
							<div class="alert alert-danger error-cont disp-none">
								<span class="glyphicon glyphicon-exclamation-sign"></span>
								<span>Error:</span>
								<span class="error-msg"></span>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="well cont-trackNum form-horizontal disp-none">
				<h3 class="text-center">Order Details</h3>
				<div class="form-group">
					<div class="col-sm-2 col-sm-offset-1 col-xs-12">
						<span class="bold">Customer</span>
					</div>
					<div class="col-sm-6 col-xs-12">
						<span id="clientName"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-2 col-sm-offset-1 col-xs-12">
						<span class="bold">Purchase Date/Time</span>
					</div>
					<div class="col-sm-6 col-xs-12">
						<span id="orderDate"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-2 col-sm-offset-1 col-xs-12">
						<span class="bold">Order Id</span>
					</div>
					<div class="col-sm-6 col-xs-12">
						<span id="orderId"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-2 col-sm-offset-1">
						<span class="bold">Tracking Number</span>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input class="form-control" id="trackNum" type="text" readonly>
					</div>
					<div class="col-sm-2 col-xs-12">
						<a class="btn btn-default clip-btn" data-clipboard-target="#trackNum" data-toggle="popover" data-placement="bottom" data-content="Copied!">Copy to clipboard</a>
					</div>
				</div>
			</div>

			<div class="usps alert alert-info disp-none">
				<!-- <iframe src="https://tools.usps.com/go/TrackConfirmAction_input" class="embed-responsive-item" scrolling="no" height="1500px" width="100%"></iframe> -->
				<h3 class="text-center"><a href="https://tools.usps.com/go/TrackConfirmAction_input" class="btn btn-warning btn-lg" target="_blank">USPS</a></h3>
				<h4 class="text-center">Please follow the above link and paste the tracking number to check the status of your package</h4>
			</div>

			<div class="13-ten alert alert-info disp-none">
				<h3 class="text-center"><a href="http://www.13-ten.com/" class="btn btn-warning btn-lg" target="_blank">13-ten</a></h3>
				<h4 class="text-center">Please follow the above link and paste the tracking number to check the status of your package</h4>
			</div>

			<div class="royal alert alert-info disp-none">
				<h3 class="text-center"><a href="https://www.royalmail.com/track-your-item" class="btn btn-warning btn-lg" target="_blank">Royalmail</a></h3>
				<h4 class="text-center">Please follow the above link and paste the tracking number to check the status of your package</h4>
			</div>
		</div>
	</div>

	<div class="spacer"></div>
    <div class="container">
        <hr>
        <div class="row-new2 row-none"> <div class="row last-banner">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <img src="http://www.maxslim.store/wp-content/uploads/2018/05/blot.png" class="img-responsive center-block" alt="Banner Image">
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h2 class="blue font40">
                    <span class="special-font extra-bold block-span">START YOUR JOURNEY TODAY! TRY PHEN375!</span>
                </h2>
                <h3 class="bold">No Prescription Required!</h3>
                <a href="http://www.maxslim.store/shop" class="button button-links">Rush My Order</a>
                <div style="margin-top: 30px;">
                    <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                        <img name="trustseal" alt="Security Seals" style="border: 0; width: 120px;" src="<?php echo get_theme_file_uri(); ?>/assets/def/img/6121-lg.gif" />
                    </a>
                </div>

                <span class="block-span">
                                      </span>
            </div>
           <!--  <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padd">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/fad.png" class="img-responsive seal-img" alt="Made in a FDA Approved Facility">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in a FDA</span>
                        <span class="dark-blue block-span special-font bold">Approved Facility</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img2.png" class="img-responsive seal-img" alt="100% Quality Guaranteed">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">100% Quality</span>
                        <span class="dark-blue block-span special-font bold">Guaranteed</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img3.png" class="img-responsive seal-img" alt="Made in USA">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in</span>
                        <span class="dark-blue block-span special-font bold">USA</span>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4 col-xs-12">
            <div class="row">
                <!-- <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-6 seals">
                                            <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
                            <tr>
                                <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
                                </td>
                            </tr>
                        </table>
                                    </div> -->

                <!-- <div class="col-md-12 col-sm-12 col-xs-1 seals">
                    <center>
                        <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                            <img name="trustseal" alt="Security Seals" style="border: 0; width: 160px;" src="//dw26xg4lubooo.cloudfront.net/seals/stacked/6121-lg.gif" />
                        </a>
                    </center>
                </div> -->
            </div>
        </div>
</div></div>
    </div>
    <div class="spacer"></div>
</div>						<!-- End Content -->

<?php get_footer( 'home' ); ?>