<?php /* Template Name: Guarantee */ 
	get_header( 'home' );
?>
								
								<div class="container">
										<div class="company-logo-top">
												<img src="../../dc411ibrlpprl.cloudfront.net/company-logo-top.jpg" alt="Matrix" id="tag" />										</div>

										
										<div class="visible-sm-12 menu-phone top-link">
												<div class="col-sm-12">
														<span><a href="track-my-order.html">Track My Order</a></span> |
														<span><a href="contact-us.html">Contact Us</a></span>
												</div>
												<div class="clearfix center-block select-container">
														<div class="pull-left">
																																<form  method="post">
																		<select id="currchangePhone" name="ChangeCurrency" onchange="this.form.submit()">
																				<option value="USD" selected='selected'>USD</option>
																				<option value="EUR" >EUR</option>
																				<option value="GBP" >GBP</option>
																				<option value="AUD" >AUD</option>
																				<option value="MXN" >MXN</option>
																				<option value="CAD" >CAD</option>
																		</select>
																</form>
														</div>
														<div class="pull-left">
																<select id="changLangPhone">
																		<option value='https://www.phen375.com/en/guarantee.html' selected='selected'>English</option>
																		<option value='https://www.phen375.com/fr/guarantee.html' >Francais</option>
																<!--Nestrix | Ing. Franco Salas-->
																		<!--Set Deutsch Language-->
																		<option value='https://www.phen375.com/de/guarantee.html' >Deutsch</option>
																		<!--Set Greek Language-->
																		<option value='https://www.phen375.com/el/guarantee.html' >Ελληνική</option>
																		<!--Set Spanish Language-->
																		<option value='https://www.phen375.com/es/guarantee.html' >Español</option>
																		<!--Set Italian Language-->
																		<option value='https://www.phen375.com/it/guarantee.html' >Italiano</option>
																		<!--Set Dutch Language-->
																		<option value='https://www.phen375.com/nl/guarantee.html' >Dutch</option>
																<!--End Modifications-->
																</select>
														</div>
												</div>
										</div>

										<div class="containerMenuMobile overContentRelative">
											
																						<div id="menuMobile" class="overContent"></div>

											<!-- <div class="fullWidth fullHeight overContentRelative">
												<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
												</div>
											</div> -->
											<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
																									<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
														<tr>
															<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
															</td>
														</tr>
													</table>
																							</div>
											<a href="http://www.maxslim.store/shop" class="button inlineBlock overContent overContent-topRight containerMenuMobile-btnOrderNow">Order Now</a>										</div>

										
								</div>
						</div>
						<!-- End Header -->

						<!-- Content -->
						
						<link rel="canonical" href="guarantee.html"/>
<div class="wrapper">
   <!-- content -->
   <div class="container content">
      <div class="container">
         <div class="row-new">

            <div class="col-md-12 feature-text">

               <h1>Phen375 Guarantee</h1>

               <p>Take Phen375 and follow our diet plan – if you do not lose weight in 30 days (see note below), we will refund the purchase price as per the <a href="return-policy.html">Return Policy</a> as stated on this website.
                  The <a href="terms-of-use.html">Terms and Conditions</a> you agreed to when buying Phen375 still apply.</p>

               <p>In order to make Phen375's money-back guarantee effective you must adhere to the following steps:</p>

               <ol>
                  <li>Download the diet plan that is pertinent to you from our website. You can find the diet plans following this link: <a href="bonus.html">Bonus</a>.</li>
                  <li>Log your initial weight before taking Phen375.</li>
                  <li>Follow the diet plan and keep track of your progress.</li>
                  <li>Log your end-weight upon completing the 30-day period of time.</li>
                  <!-- <li>If you have not experienced any weight-loss after taking Phen375 and following our diet plan in 30 days, visit a certified doctor present him or her with supporting documentation and request a reliable statement proving there hasn't been any weight loss.</li> -->
                  <li>
                     <!--   If you have not experienced any weight-loss after taking Phen375 and following our diet plan in 30 days, send us a written sworn statement as credible proof that there has been no weight-loss. -->
                     If you have not experienced any weight-loss after taking Phen375 and following our diet plan in 30 days, send us a written, signed and scanned sworn statement, front and back scanned images of government-issued ID to verify signature on the statement and time-stamped before and after photos as credible proof that there hasn't been any weight-loss.
                  </li>


                  <li>Phen375 reserves the right to upgrade your order to the newest version.</li>
               </ol>

               <p>At the end of the 30-day period you have up to 30 days to claim the money back guarantee alongside supporting documentation and reliable certified statement.</p>
               <p>The 60-day time frame starts from the date the product was ordered. Shipping and handling, postal and inspection charges are non-refundable in accordance to our <a href="return-policy.html">Return Policy</a>. </p>

               <!--  <div class="alert alert-success">
                  <span class="block-span"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Please note:</span>
                  <h2>We will not accept money-back guarantee claims without a reliable certified statement.</h2>
               </div> -->

               <p>We have many satisfied customers around the world, thanks to Phen375's ability to help people meet their weight-loss goals. The measures taken in steps 1 to 5 of the money-back guarantee were designed to stop fraud. We thank you for understanding and for your interest in Phen375.</p>

            </div>

         </div>
         <!-- /.feature-text -->
      </div>
      <hr>
      <div class="row"> <div class="row last-banner">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <img src="http://www.maxslim.store/wp-content/uploads/2018/05/blot.png" class="img-responsive center-block" alt="Banner Image">
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h2 class="blue font40">
                    <span class="special-font extra-bold block-span">START YOUR JOURNEY TODAY! TRY PHEN375!</span>
                </h2>
                <h3 class="bold">No Prescription Required!</h3>
                <a href="http://www.maxslim.store/shop" class="button button-links">Rush My Order</a>
                <div style="margin-top: 30px;">
                    <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                        <img name="trustseal" alt="Security Seals" style="border: 0; width: 120px;" src="<?php echo get_theme_file_uri(); ?>/assets/def/img/6121-lg.gif" />
                    </a>
                </div>

                <span class="block-span">
                                      </span>
            </div>
           <!--  <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padd">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/fad.png" class="img-responsive seal-img" alt="Made in a FDA Approved Facility">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in a FDA</span>
                        <span class="dark-blue block-span special-font bold">Approved Facility</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img2.png" class="img-responsive seal-img" alt="100% Quality Guaranteed">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">100% Quality</span>
                        <span class="dark-blue block-span special-font bold">Guaranteed</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img3.png" class="img-responsive seal-img" alt="Made in USA">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in</span>
                        <span class="dark-blue block-span special-font bold">USA</span>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4 col-xs-12">
            <div class="row">
                <!-- <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-6 seals">
                                            <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
                            <tr>
                                <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
                                </td>
                            </tr>
                        </table>
                                    </div> -->

                <!-- <div class="col-md-12 col-sm-12 col-xs-1 seals">
                    <center>
                        <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                            <img name="trustseal" alt="Security Seals" style="border: 0; width: 160px;" src="//dw26xg4lubooo.cloudfront.net/seals/stacked/6121-lg.gif" />
                        </a>
                    </center>
                </div> -->
            </div>
        </div>
</div></div>
      <!-- <div class="spacer"></div> -->
   </div>
</div>						<!-- End Content -->

<?php get_footer( 'home' ); ?>