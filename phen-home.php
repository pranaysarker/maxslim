<?php
	/* Template Name: HomePage */
?>
<?php get_header( 'home' ); ?>
								<div class="container">
										<div class="company-logo-top">
												<img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/company-logo-top.jpg" alt="Matrix" id="tag" />										</div>

										
										<div class="visible-sm-12 menu-phone top-link">
												<div class="col-sm-12">
														<span><a href="track-my-order.html">Track My Order</a></span> |
														<span><a href="contact-us.html">Contact Us</a></span>
												</div>
												<div class="clearfix center-block select-container">
														<div class="pull-left">
																																<form  method="post">
																		<select id="currchangePhone" name="ChangeCurrency" onchange="this.form.submit()">
																				<option value="USD" selected='selected'>USD</option>
																				<option value="EUR" >EUR</option>
																				<option value="GBP" >GBP</option>
																				<option value="AUD" >AUD</option>
																				<option value="MXN" >MXN</option>
																				<option value="CAD" >CAD</option>
																		</select>
																</form>
														</div>
														<div class="pull-left">
																<select id="changLangPhone">
																		<option value='https://www.phen375.com/en/index.html' selected='selected'>English</option>
																		<option value='https://www.phen375.com/fr/index.html' >Francais</option>
																<!--Nestrix | Ing. Franco Salas-->
																		<!--Set Deutsch Language-->
																		<option value='https://www.phen375.com/de/index.html' >Deutsch</option>
																		<!--Set Greek Language-->
																		<option value='https://www.phen375.com/el/index.html' >Ελληνική</option>
																		<!--Set Spanish Language-->
																		<option value='https://www.phen375.com/es/index.html' >Español</option>
																		<!--Set Italian Language-->
																		<option value='https://www.phen375.com/it/index.html' >Italiano</option>
																		<!--Set Dutch Language-->
																		<option value='https://www.phen375.com/nl/index.html' >Dutch</option>
																<!--End Modifications-->
																</select>
														</div>
												</div>
										</div>

										<div class="containerMenuMobile overContentRelative">
											
																						<div id="menuMobile" class="overContent"></div>

											<!-- <div class="fullWidth fullHeight overContentRelative">
												<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
												</div>
											</div> -->
											<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
																									<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
														<tr>
															<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
															</td>
														</tr>
													</table>
																							</div>
											<a href="http://www.maxslim.store/shop" class="button inlineBlock overContent overContent-topRight containerMenuMobile-btnOrderNow">Order Now</a>										</div>

										
								</div>
						</div>
						<!-- End Header -->

						<!-- Content -->
						
						<link rel="canonical" href="index.html"/>
 
<div class="container">
    <div class="row main-banner">
        <div class="col-md-3 col-sm-3 col-xs-12 mt-15">
            <img src="http://www.maxslim.store/wp-content/uploads/2018/05/phen375_home_mbgc-1.png" alt="Phen375 bottle" class="img-responsive center-block">
        </div>
        <div class="col-md-4 col-sm-5 col-xs-12 no-padd sub-container">
            <h1 class="blue">
                <span class="block-span special-font extra-bold text-uppercase">Lose Weight</span>
                <span class="block-span special-font light text-uppercase">Faster &amp; Easier</span>
                <span class="block-span special-font light text-uppercase">Than Ever Before</span>
            </h1>
            <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/newphen375/border.png" class="img-responsive" alt="border green">
            <p class="special-font semi-bold">Improving your metabolism and suppressing your hunger will increase your body's ability to burn fat!</p>
            <ul class="features special-font">
                <li><img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/newphen375/scale.png" class="img-responsive" alt="scale"> <span>Highly refined ingredients!</span></li>
                <li><img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/newphen375/apple.png" class="img-responsive" alt="apple"> <span>May Reduce Food Cravings!</span></li>
                <li><img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/newphen375/tape.png"  class="img-responsive" alt="tape"> <span>Increasing Metabolism can Burn Fat!</span></li>
            </ul>
        </div>
        <div class="col-md-5 col-sm-4 col-xs-12 no-padd">
            <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/newphen375/img5.png" class="img-responsive pull-right" alt="Phen375 - girl" />        </div>
    </div>
    <div class="row blue-bk">
        <div class="col-md-7 col-sm-7 col-xs-12 white blue-banner-text">
            <!-- Hay que trabajar Aqui -->
            <h2 class="special-font bold">Burning fat daily is possible with an increased metabolism!</h2>
            <span class="block-span special-font light">Get started Now!</span>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 order-now">
            <a href="http://www.maxslim.store/shop" class="button button-links">Order Your Supply Today!</a>        </div>
    </div>

    <div class="row">
        <div class="col-md-7 col-sm-7 col-xs-12 slimer-container">
            <h3 class="blue special-font semi-bold">Become a Slimmer, More Sexy You!</h3>

            <p class="special-font">
                <!-- Imagine yourself becoming slimmer and getting in shape. Any doctor or dietitian will tell you that in order to lose weight, you have to reduce caloric intake, eat nutritious foods and exercise regularly. Losing the extra pounds will not only make you feel better, but you will also look better. -->
                Imagine yourself becoming slimmer and getting in shape!
                Losing the extra pounds may not only make you feel better, but also may, help you look better.
            </p>
            <p class="special-font semi-bold">
                <!-- Phen375 is a dietary supplement designed for weight loss and hunger suppression! -->
                Phen375 is a dietary supplement that may lead to weight and hunger suppression!
            </p>
            <p class="special-font">Consuming excessive calories without burning it off normally results in weight gain. Phen375 has specifically-designed diet plans and exercise routines created to help you burn fat.</p>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 no-padd">
            <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/phen375/images/phen375_woman_bottlesc.jpg" class="img-responsive pull-right" alt="Banner Image">
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 benefit-container">
            <h3 class="blue text-center">Suppress Cravings, Burn Fat &amp; Lose Weight</h3>

            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/scale2.png" alt="icon" class="img-responsive center-block">
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 benefit-content">
                    <span>Calorie Reduction ,</span>
                    <span>Leads To Weight Loss</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/newphen375/icon2.png" alt="icon" class="img-responsive center-block">
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 benefit-content">
                    <span>Avoid Pricey Liposuction</span>
                    <span>And Spend Less Money</span>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/phen375/images/icon3.png" alt="icon" class="img-responsive center-block">
                </div>
                <div class="col-md-8 col-sm-8 col-xs-12 benefit-content">
                    <span>High Quality Ingredients</span>
                </div>
            </div>
        </div>
    </div>

<!-- New Section -->
    <div class="row sectionSpace mar-30">
        <div class="col-md-12 text-center">
            <a href="http://www.maxslim.store/shop" class="button button-links inlineBlock">Try Phen375 Today!</a>        </div>
    </div>

    <div class="row sectionBoxs sectionSpace">
        <div class="text-center sectionBoxs__box col-md-5ths col-xs-6 overContentRelative securyPurchase__box__norton">
            <div class="securyPurchase__box__inner overContent overContent-centerAll securyPurchase__box__inner--norton">
                <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/homepage-v2/checknorton_42px.png" class="img-responsive" alt="norton">
                <p>
                    <span> Norton </span>
                    <sup>TM</sup>
                </p>
            </div>

        </div>
        <div class="text-center sectionBoxs__box col-md-5ths col-xs-6 overContentRelative securyPurchase__box_shopping">
            <div class="securyPurchase__box__inner securyPurchase__box__inner--shopping overContent overContent-centerAll">
                <p>SHOPPING</p>
                <p class="noMargin">GUARANTEE</p>
            </div>
        </div>
        <div class="sectionBoxs__box col-md-5ths securyPurchase__box col-xs-6 overContentRelative">
            <div class="securyPurchase__box__inner overContent overContent-centerAll">
                <div>
                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/homepage-v2/icon-3.png" class="img-responsive" alt="seal">
                    <strong class="securyPurchase__box__title">$10,000</strong>
                </div>
                <p class="noMargin securyPurchase__box__body">ID theft</p>
                <p class="noMargin securyPurchase__box__body">Protection</p>
            </div>

        </div>
        <div class="sectionBoxs__box col-md-5ths securyPurchase__box col-xs-6 overContentRelative">
            <div class="securyPurchase__box__inner overContent overContent-centerAll ">
                <div>
                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/homepage-v2/icon-5.png" class="img-responsive" alt="seal">
                    <strong class="securyPurchase__box__title">$1,000</strong>
                </div>
                <p class="noMargin securyPurchase__box__body">Purchase</p>
                <p class="noMargin securyPurchase__box__body">Guarantee</p>
            </div>
        </div>
        <div class="sectionBoxs__box col-md-5ths securyPurchase__box col-xs-6 overContentRelative"
            style="border-radius: 0 0 22px 0;">
            <div class="securyPurchase__box__inner overContent overContent-centerAll ">
                <div>
                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/homepage-v2/icon-4.png" class="img-responsive" alt="seal">
                    <strong class="securyPurchase__box__title">$100</strong>
                </div>
                <p class="noMargin securyPurchase__box__body">Lowest Price</p>
                <p class="noMargin securyPurchase__box__body">Guarantee</p>
            </div>
        </div>
    </div>
    <div class="row sectionCustomerMillion sectionSpace">
        <div class="col-md-6 col-sm-6 col-xs-12 customerMillion">
            <div class="customerMillion__img">
                <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/homepage-v2/icon-1.png" class="img-responsive">
            </div>
            <div class="customerMillion__number">
                <span>152.8</span>
            </div>
            <div class="customerMillion__text">
                <p class="noMargin">Million</p>
                <p class="noMargin">Unique Visitors</p>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 customerMillion">
            <div class="customerMillion__img">
                <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/homepage-v2/icon-2.png" class="img-responsive">
            </div>
            <div class="customerMillion__number">
                <span>256,138</span>
            </div>
            <div class="customerMillion__text">
                <span>Completed</span>
                <br>
                <span>Customer Surveys</span>
            </div>
        </div>
    </div>
    <div class="row sectionSpace">
        <div class="col-md-12">
            <h3 class="blue special-font text-center">
                Become a Slimmer, Sexier More Attractive You!
            </h3>
        </div>
        <div class="col-md-12">
            <p class="text-center noMargin">
                <strong>
                    <!-- Imagine yourself becoming slimmer and getting in shape in No-Time! -->
                    Imagine yourself becoming slimmer and getting in shape!
                </strong>
            </p>
            <p class="text-center">
                <strong>
                    Losing the extra pounds may not only make you feel better but also may, help you look better.
                    <!-- Losing those extra pounds will not only make you look better but you will also feel better! -->
                </strong>
            </p>
        </div>
    </div>
    <div class="row sectionSpace">
        <div class="col-md-4 col-sm-4 sectionCustomerHappy centerBlock">
            <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/newphen375/testimony-image-1.jpg" class="img-responsive centerBlock" alt="Catherine's testimonial">
            <p class="sectionCustomerHappy__testimonial text-justify">
                <br>
                <!-- I took Phen375 for 60 days. I dropped down from 140 pounds to my ideal weight of 115. I'm happier, have more energy and owe it all to this amazing diet pill! 100% recommended. -->
                <br><span class="centerBlock text-center">Catherine, USA</span>
            </p>
        </div>
        <div class="col-md-4 col-sm-4 sectionCustomerHappy centerBlock">
            <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/newphen375/testimony-image-2.jpg" class="img-responsive centerBlock" alt="Denise's testimonial">
            <p class="sectionCustomerHappy__testimonial text-justify">
                <br>
                <!-- My starting weight before Phen375 was 148 lbs. I was always uncomfortable in any clothes. After taking these pills I dropped 20 lbs. almost immediately. Now I look amazing in any clothes! -->
                <br><span class="centerBlock text-center">Denise, USA</span>
            </p>
        </div>
        <div class="col-md-4 col-sm-4 sectionCustomerHappy centerBlock">
            <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/newphen375/testimony-image-3.jpg" class="img-responsive centerBlock" alt="Evelyn's testimonial">
            <p class="sectionCustomerHappy__testimonial text-justify">
                <br>
              <!--   I started at 130 and for my frame that is pretty heavy! I thought it would take forever to get to my goal of 105 lbs. but I did it in just 40 days. I feel good and I look way better now. -->
                <br><span class="centerBlock text-center">Evelyn, USA</span>
            </p>
        </div>
    </div>
    <div class="row sectionSpace">
        <div class="col-md-12 text-center">
            <a href="http://www.maxslim.store/shop" class="button button-links inlineBlock">Order Your Supply Today!</a>        </div>
    </div>
    <div class="row sectionSpace sectionCustomerCounter">
        <div class="col-md-8 col-xs-12 sectionCustomerCounter__text">
            <h2 class="noMargin blue">
                <span class="special-font extra-bold">
                    <!-- HAPPY CUSTOMERS BUYING -->
                    CUSTOMERS BUYING
                </span>
            </h2>
            <h4 class="noMargin blue">
                <span class="special-font extra-bold">
                    RIGHT NOW! TRY PHEN375!
                </span>
            </h4>
        </div>
        <div class="col-md-4 col-xs-12 text-center">
            <div class="timer counter counter-analog fullWidth"></div>
        </div>
    </div>
<!-- End new section -->

    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12 grey-banner">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-4">
                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/newphen375/1.png" class="img-responsive center-block" alt="icon">
                </div>
                <div class="col-md-9 col-sm-9 col-xs-8">
                    <span class="block-span special-font extra-bold">Calorie reduction!</span>
                    <span>Leads to weight loss</span>
                </div>
            </div>

            <div class="row mt-15">
                <div class="col-md-3 col-sm-3 col-xs-4">
                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/No_prescription_required.png" class="img-responsive center-block" alt="icon">
                </div>
                <div class="col-md-9 col-sm-9 col-xs-8">
                    <span class="block-span special-font extra-bold">No prescription required</span>
                    <span>At less than $3.80/day!</span>
                </div>
            </div>

            <div class="row mt-15">
                <div class="col-md-3 col-sm-3 col-xs-4">
                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/newphen375/3.png" class="img-responsive center-block" alt="icon">
                </div>
                <div class="col-md-9 col-sm-9 col-xs-8">
                    <span class="block-span special-font extra-bold">Increasing your metabolism </span>
                    <span>May boost your energy!</span>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12 change-life">
            <h3 class="blue special-font">You Can Change Your Life Now!</h3>

            <p class="mt-15">Doctor's, dieticians and other experts agree that the best way to lose weight is to eat fewer calories and be more active</p>
            <p>For most people, a reasonable goal is to lose about a pound a week, which means, cutting about 500 calories a day from your diet, eating a variety of nutritious foods and exercising regularly!</p>

            <p class="special-font semi-bold">Let Phen375 assist you in your weight-loss journey!</p>
            <p>
                If you buy now you can take advantage of the free diet plan and cellulite reduction guide being offered for a limited time when you purchase Phen375.
            </p>
        </div>
    </div>
    <div class="">
    
        <div class="row"> <div class="row last-banner">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <img src="http://www.maxslim.store/wp-content/uploads/2018/05/blot.png" class="img-responsive center-block" alt="Banner Image">
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h2 class="blue font40">
                    <span class="special-font extra-bold block-span">START YOUR JOURNEY TODAY! TRY PHEN375!</span>
                </h2>
                <h3 class="bold">No Prescription Required!</h3>
                <a href="http://www.maxslim.store/shop" class="button button-links">Rush My Order</a>
                <div style=
                "margin-top: 30px;">
                    <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                        <img name="trustseal" alt="Security Seals" style="border: 0; width: 120px;" src="<?php echo get_theme_file_uri(); ?>/assets/def/img/6121-lg.gif" />
                    </a>
                </div>

                <span class="block-span">
                                      </span>
            </div>
           <!--  <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padd">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/fad.png" class="img-responsive seal-img" alt="Made in a FDA Approved Facility">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in a FDA</span>
                        <span class="dark-blue block-span special-font bold">Approved Facility</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img2.png" class="img-responsive seal-img" alt="100% Quality Guaranteed">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">100% Quality</span>
                        <span class="dark-blue block-span special-font bold">Guaranteed</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img3.png" class="img-responsive seal-img" alt="Made in USA">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in</span>
                        <span class="dark-blue block-span special-font bold">USA</span>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4 col-xs-12">
            <div class="row">
                <!-- <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-6 seals">
                                            <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
                            <tr>
                                <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
                                </td>
                            </tr>
                        </table>
                                    </div> -->

                <!-- <div class="col-md-12 col-sm-12 col-xs-1 seals">
                    <center>
                        <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                            <img name="trustseal" alt="Security Seals" style="border: 0; width: 160px;" src="//dw26xg4lubooo.cloudfront.net/seals/stacked/6121-lg.gif" />
                        </a>
                    </center>
                </div> -->
            </div>
        </div>
</div></div>
        
    </div>
</div>
						<!-- End Content -->

<?php get_footer( 'home' ); ?>