<?php /* Template Name: Return-policy */
	get_header( 'home' );
 ?>
								
								<div class="container">
										<div class="company-logo-top">
												<img src="../../dc411ibrlpprl.cloudfront.net/company-logo-top.jpg" alt="Matrix" id="tag" />										</div>

										
										<div class="visible-sm-12 menu-phone top-link">
												<div class="col-sm-12">
														<span><a href="track-my-order.html">Track My Order</a></span> |
														<span><a href="contact-us.html">Contact Us</a></span>
												</div>
												<div class="clearfix center-block select-container">
														<div class="pull-left">
																																<form  method="post">
																		<select id="currchangePhone" name="ChangeCurrency" onchange="this.form.submit()">
																				<option value="USD" selected='selected'>USD</option>
																				<option value="EUR" >EUR</option>
																				<option value="GBP" >GBP</option>
																				<option value="AUD" >AUD</option>
																				<option value="MXN" >MXN</option>
																				<option value="CAD" >CAD</option>
																		</select>
																</form>
														</div>
														<div class="pull-left">
																<select id="changLangPhone">
																		<option value='https://www.phen375.com/en/return-policy.html' selected='selected'>English</option>
																		<option value='https://www.phen375.com/fr/return-policy.html' >Francais</option>
																<!--Nestrix | Ing. Franco Salas-->
																		<!--Set Deutsch Language-->
																		<option value='https://www.phen375.com/de/return-policy.html' >Deutsch</option>
																		<!--Set Greek Language-->
																		<option value='https://www.phen375.com/el/return-policy.html' >Ελληνική</option>
																		<!--Set Spanish Language-->
																		<option value='https://www.phen375.com/es/return-policy.html' >Español</option>
																		<!--Set Italian Language-->
																		<option value='https://www.phen375.com/it/return-policy.html' >Italiano</option>
																		<!--Set Dutch Language-->
																		<option value='https://www.phen375.com/nl/return-policy.html' >Dutch</option>
																<!--End Modifications-->
																</select>
														</div>
												</div>
										</div>

										<div class="containerMenuMobile overContentRelative">
											
																						<div id="menuMobile" class="overContent"></div>

											<!-- <div class="fullWidth fullHeight overContentRelative">
												<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
												</div>
											</div> -->
											<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
																									<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
														<tr>
															<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
															</td>
														</tr>
													</table>
																							</div>
											<a href="http://www.maxslim.store/shop" class="button inlineBlock overContent overContent-topRight containerMenuMobile-btnOrderNow">Order Now</a>										</div>

										
								</div>
						</div>
						<!-- End Header -->

						<!-- Content -->
						
						      <link rel="canonical" href="return-policy.html" />
      <div class="wrapper">
         <!-- content -->
         <div class="container content">
            <div class="container">
               <div class="row-new"><div class="col-md-12 feature-text">

                  <h1>Return Policy</h1>

                  <p>All orders are final, once the order is made it is no longer returnable unless authorized by the support and logistics department.</p>

                  <p>We have developed and used automated systems to ship products as quickly as possible to ensure timely reception of your order. If you have placed an order and wish to have it canceled, we will do our very best to process your cancellation request prior to your order shipping. </p>

                  <p>If your order has already shipped, the cancelation of the order cannot be effective. </p>

                  <!-- <p>All returns must be authorized by support within the 30 days of ordering, refunds that are authorized by support contain (RMA) numbers issued and are subject to a $45 package inspection fee to cover hazmat protocol for human consumption items, $14shipping fee if USPS shipping was used International orders will have a $45 deducted to cover cost of shipping and handling applied all orders returned will also be charged a 18% restocking fee. RMA number is required for all returns without exception.</p> -->
                  <p>A product can then only be returned once our qualified support staff issues a Return Merchandise Authorization (RMA). Purchases can only be returned within 48 hours of arrival. </p>

                  <p>Once an authorization has been provided you will need to do the following steps:</p>
                  <ol>
                     <li><p>Return the package to the address given by the support representative.</p></li>
                     <li><p>All returned packages must be via Priority mail with a tracking number.</p></li>
                     <li><p>We highly recommend that when returning a package you insure it for its purchase price.</p></li>

                  </ol>
                  <p>Insuring your package will minimize your liability while in transit, ensuring that any damage such as the box being damaged or any other circumstance during transit will be covered by the insurance you obtained.</p>

                 <!--  <p>Please notice that removal or alteration of a product's seal i.e. sticker, will automatically be considered as a consumed item and void any possible refunding available under our Return policy. </p> -->
                  <p>Please make sure you follow the recommendations below: </p>
                  <ol>
                     <li><p>Fully inspect all deliveries before signing the receipt even if you do not plan on opening the package right away.</p></li>
                     <li><p>Report any visible external damage to the product upon arrival contact our customer support team, providing a picture of the product.</p></li>
                     <li><p>If the product does not appear to be suitable for consumption contact our customer support team, providing a picture of the product.</p></li>
                  </ol>
                  <p>Products that are returned without a Return Merchandise Authorization issued by our Support Team will not be accepted. This also applies to packages returned without an RMA and that are damaged, altered, have broken seals and labels are considered non-resalable or not suitable for consumption. The aforementioned conditions voids the return policy. </p>

                  <p><strong>Shipping and handling, postal and inspection charges are non-refundable.</strong></p>
               </div></div>

               <!-- /.feature-text -->
            </div>
            <hr>
             <div class="row"> <div class="row last-banner">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <img src="http://www.maxslim.store/wp-content/uploads/2018/05/blot.png" class="img-responsive center-block" alt="Banner Image">
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h2 class="blue font40">
                    <span class="special-font extra-bold block-span">START YOUR JOURNEY TODAY! TRY PHEN375!</span>
                </h2>
                <h3 class="bold">No Prescription Required!</h3>
                <a href="http://www.maxslim.store/shop" class="button button-links">Rush My Order</a>
                <div style="margin-top: 30px;">
                    <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                        <img name="trustseal" alt="Security Seals" style="border: 0; width: 120px;" src="<?php echo get_theme_file_uri(); ?>/assets/def/img/6121-lg.gif" />
                    </a>
                </div>

                <span class="block-span">
                                      </span>
            </div>
           <!--  <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padd">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/fad.png" class="img-responsive seal-img" alt="Made in a FDA Approved Facility">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in a FDA</span>
                        <span class="dark-blue block-span special-font bold">Approved Facility</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img2.png" class="img-responsive seal-img" alt="100% Quality Guaranteed">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">100% Quality</span>
                        <span class="dark-blue block-span special-font bold">Guaranteed</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img3.png" class="img-responsive seal-img" alt="Made in USA">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in</span>
                        <span class="dark-blue block-span special-font bold">USA</span>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4 col-xs-12">
            <div class="row">
                <!-- <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-6 seals">
                                            <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
                            <tr>
                                <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
                                </td>
                            </tr>
                        </table>
                                    </div> -->

                <!-- <div class="col-md-12 col-sm-12 col-xs-1 seals">
                    <center>
                        <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                            <img name="trustseal" alt="Security Seals" style="border: 0; width: 160px;" src="//dw26xg4lubooo.cloudfront.net/seals/stacked/6121-lg.gif" />
                        </a>
                    </center>
                </div> -->
            </div>
        </div>
</div></div>
            
         </div>
      </div>						<!-- End Content -->

<?php get_footer( 'home' ); ?>