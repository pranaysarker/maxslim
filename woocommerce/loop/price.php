<?php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product,$WC;
?>

<?php if ( $price_html = $product->get_price_html() ) : ?>

	                <span class="d-block regular-tagline bold text-uppercase">Regular Price</span>
	                <span class="d-block regular-price bold" style="text-decoration: line-through; font-size: 25px;">$<?php echo $product->get_regular_price(); ?></span>
	                <h3 class="sels-price bold">$<?php echo $product->get_sale_price(); ?></h3>
	                <?php do_action( 'woocommerce_before_cart_totals' ); ?>
	                <p class="hands">+S&amp;H 		

						<?php 
						echo cs_get_option('product_shiping_static');

						// ob_start();

						// wc_cart_totals_shipping_html(); 
						// $maxslimShipping = ob_get_clean();
						// $maxslimShipping = str_replace('Shipping', ' ', $maxslimShipping);
						// $maxslimShipping = str_replace('Flat', ' ', $maxslimShipping);
						// $maxslimShipping = str_replace('rate:', ' ', $maxslimShipping);
						// if( $maxslimShipping ){
						// 	echo $maxslimShipping;
						// }else{
						// 	echo '$19.25';
						// }

						?>
					</p>





			



















































	<!-- <span class="price"><?php // echo $price_html; ?></span> -->

<?php endif; ?>
