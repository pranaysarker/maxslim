<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>

<div class="maxslim-product-container">
	<div id="post-id-<?php get_the_ID(); ?>" class="maxslim-product">

		<div class="product-top">
			<div class="title">
	<?php 

		/**
		 * woocommerce_before_shop_loop_item_title hook.
		 *
		 * @hooked woocommerce_show_product_loop_sale_flash - 10
		 * @hooked woocommerce_template_loop_product_thumbnail - 10
		 */
		// do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	do_action( 'woocommerce_shop_loop_item_title' );

	?>
			</div>
			<?php if($product->get_attribute( 'best-value' ) == 'yes' ): ?>
			<div class="best-value">
				<img src="<?php echo get_theme_file_uri( 'assets/images/best-value.png' ); ?>" alt="">
			</div>
			<?php endif; ?>
		</div>

		<div class="product-content row">
			<div class="product-image saving col-md-8">
				<?php do_action( 'woocommerce_before_shop_loop_item_title' ); ?>
				<div class="totla-saving text-center">
					<span class="d-block bold text-small">Total</span>
					<span class="d-block bold text-small">Savings</span>
					<span class="d-block bold">$<?php echo $product->get_regular_price() - $product->get_sale_price(); ?></span>
				</div>
			</div>
			<div class="product-price col-md-4 text-right pt-4 pr-4">
					<?php woocommerce_template_loop_price(); ?>
					
	                <div class="select right">
						<?php woocommerce_template_loop_add_to_cart(); ?>
	                </div>
	                <div class="clear"></div>
	            
			</div>
		</div>
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );



	/**
	 * woocommerce_after_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	// do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );
	?>
	</div>
</div>
