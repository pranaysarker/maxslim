var languageText = {};
(function(){
    //Private variable
    var that = this;
    this.language = {
        orderNow : {
            en: "Order Now",
            it: "Ordina Ora",
            fr: "Commandez",
            el: "Παραγγελία",
            es: "Pedido Ahora",
            de: "Bestellen"
        }
    }

    this.getTextByLanguage = function(text, language){
        var textObj = this.language[text];
        return textObj[language];
    };
    return this;

}).call(languageText);