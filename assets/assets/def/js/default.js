var defaultLayout = {};
(function(){
    'use strict';
    //Private variable
    var that = this;
    this.limitScroll = 223;

    this.init = function(){
        this.loadElement();
        this.handlerScroll();
        this.showLabelOrderNow();
    }

    this.showLabelOrderNow = function(){
        var orderNowText = languageText.getTextByLanguage("orderNow",globalValues.language);
        $(".containerMenuMobile-btnOrderNow").html(orderNowText);
    };

    this.loadElement = function(){
        $(".containerMenuMobile").css({zIndex:1});
        $(".slicknav_menu").addClass("overContent-topLeft").css({top:0});
        $(".slicknav_btn").addClass("pull-left");
    };


    this.handlerScroll = function(){
        $(window).scroll(this.scroll);
    };

    this.scroll = function(event){
        var scroll = $(this).scrollTop();
        if(scroll > that.limitScroll){
            $(".containerMenuMobile").addClass('overContent-fixedTop fullWidth');
        } else {
            $(".containerMenuMobile").removeClass('overContent-fixedTop fullWidth');
        }
    };

    /*Check if the string is a valid email*/
    this.isValidEmailAddress = function(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    };

    this.readyJ = function(){
        $(window).load(that.windowLoad);
    };

    this.windowLoad = function(){
        that.init();
    };

    this.loadElement = function(){
        if(globalValues.action == "upsell" ||
           globalValues.action == "downsell"){
            $(".containerMenuMobile, .menu-phone").css({"display":"none"});

        } else {
            $(".containerMenuMobile").css({zIndex:1});
            $(".slicknav_menu").addClass("overContent-topLeft").css({top:0});
            $(".slicknav_btn").addClass("pull-left");
        }
    };

    $(document).ready(this.readyJ);

    return this;
}).call(defaultLayout);
