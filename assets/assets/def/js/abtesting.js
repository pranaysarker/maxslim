var abtesting = new function(){
    //Private variable
    var that = this;
    this.counterExecute = false;

    this.init = function(){
        this.loadElement();
        this.handlerEvent();
    };

    this.handlerEvent = function(){
        this.handlerScroll();
    };

    this.handlerScroll = function(){
        $(window).scroll(this.scroll);
    };


    this.scroll = function(event){
        var scroll = $(this).scrollTop();
        // console.log(scroll);
        if(scroll > 1500 && !that.counterExecute){
            that.counterExecute = true;
            that.executeCounter();
        }
    };

    this.executeCounter = function(){
        $('.timer').countTo({
            from: 256138,
            to: 258000,
            speed: 10000000,
            refreshInterval: 50,
            onComplete: function(value) {
                console.debug(this);
            }
        });
    };

    this.loadElement = function(){

    };

    this.jquery = function(){
        that.init();
        // that.readyJ();
    }

    $(document).ready(this.jquery);
};