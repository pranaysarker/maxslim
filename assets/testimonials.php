<?php /*Template Name: Testimonial */ ?>
								<div class="container">
										<div class="company-logo-top">
												<img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/company-logo-top.jpg" alt="Matrix" id="tag" />										</div>

										
										<div class="visible-sm-12 menu-phone top-link">
												<div class="col-sm-12">
														<span><a href="track-my-order.html">Track My Order</a></span> |
														<span><a href="contact-us.html">Contact Us</a></span>
												</div>
												<div class="clearfix center-block select-container">
														<div class="pull-left">
																																<form  method="post">
																		<select id="currchangePhone" name="ChangeCurrency" onchange="this.form.submit()">
																				<option value="USD" selected='selected'>USD</option>
																				<option value="EUR" >EUR</option>
																				<option value="GBP" >GBP</option>
																				<option value="AUD" >AUD</option>
																				<option value="MXN" >MXN</option>
																				<option value="CAD" >CAD</option>
																		</select>
																</form>
														</div>
														<div class="pull-left">
																<select id="changLangPhone">
																		<option value='https://www.phen375.com/en/testimonials.html' selected='selected'>English</option>
																		<option value='https://www.phen375.com/fr/testimonials.html' >Francais</option>
																<!--Nestrix | Ing. Franco Salas-->
																		<!--Set Deutsch Language-->
																		<option value='https://www.phen375.com/de/testimonials.html' >Deutsch</option>
																		<!--Set Greek Language-->
																		<option value='https://www.phen375.com/el/testimonials.html' >Ελληνική</option>
																		<!--Set Spanish Language-->
																		<option value='https://www.phen375.com/es/testimonials.html' >Español</option>
																		<!--Set Italian Language-->
																		<option value='https://www.phen375.com/it/testimonials.html' >Italiano</option>
																		<!--Set Dutch Language-->
																		<option value='https://www.phen375.com/nl/testimonials.html' >Dutch</option>
																<!--End Modifications-->
																</select>
														</div>
												</div>
										</div>

										<div class="containerMenuMobile overContentRelative">
											
																						<div id="menuMobile" class="overContent"></div>

											<!-- <div class="fullWidth fullHeight overContentRelative">
												<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
												</div>
											</div> -->
											<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
																									<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
														<tr>
															<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
															</td>
														</tr>
													</table>
																							</div>
											<a href="http://www.maxslim.store/shop" class="button inlineBlock overContent overContent-topRight containerMenuMobile-btnOrderNow">Order Now</a>										</div>

										
								</div>
						</div>
						<!-- End Header -->

						<!-- Content -->
						
						    <link rel="canonical" href="testimonials.html" />
    <div class="wrapper">
       <!-- content -->
        <div class="container content">
            <div class="container">
                <div class="row-new">
                    <div class="col-md-12"> <!-- -box-2-->
                        <div class="feature-text testimonial">
                            <h1>Testimonials and Success Stories</h1>
                      <!-- images -->
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_elysia.jpg" class="img-responsive" alt="Responsive image">
                                    <h3 class="testimonialName">Elysia 
                                        <!-- <span>Lost 50 lbs</span> -->
                                    </h3>
                                </div>
                                <div class="col-md-6">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_mohammad.jpg" class="img-responsive" alt="Responsive image">
                                    <h3 class="testimonialName">Mohammad 
                                        <!-- <span>Lost 99 lbs</span> -->
                                    </h3>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <div class="col-md-6">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_sarah.jpg" class="img-responsive" alt="Responsive image">
                                    <h3 class="testimonialName">Sarah 
                                        <!-- <span>Lost 44 lbs</span> -->
                                    </h3>
                                </div>
                                <div class="col-md-6">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_paul.jpg" class="img-responsive" alt="Responsive image">
                                    <h3 class="testimonialName">Paul 
                                        <!-- <span>Lost 93 lbs</span> -->
                                    </h3>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <div class="col-md-6">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_ryan.jpg" class="img-responsive" alt="Responsive image">
                                    <h3 class="testimonialName">Ryan 
                                        <!-- <span>Lost 57 lbs</span> -->
                                    </h3>
                                </div>
                                <div class="col-md-6">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_dylan.jpg" class="img-responsive" alt="Responsive image">
                                    <h3 class="testimonialName">Dylan
                                     <!-- <span>Lost 20 lbs</span> -->
                                    </h3>
                                </div>
                            </div>
                            <!-- Old Testimonials -->
                            <div class="col-md-12">
                                <hr>
                                <div class="col-md-6">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_raymond.jpg" class="img-responsive" alt="Responsive image">
                                    <h3 class="testimonialName">Raymond 
                                        <!-- <span>Lost 12 lbs</span> -->
                                    </h3>
                                </div>
                                <div class="col-md-6">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_danielle.jpg" class="img-responsive" alt="Responsive image">
                                    <h3 class="testimonialName">Danielle 
                                        <!-- <span>Lost 15 lbs</span> -->
                                    </h3>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <div class="col-md-6">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_alana.jpg" class="img-responsive" alt="Responsive image">
                                    <h3 class="testimonialName">Alana 
                                        <!-- <span>Lost 10 lbs</span> -->
                                    </h3>
                                </div>
                                <div class="col-md-6">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_tristan.jpg" class="img-responsive" alt="Responsive image">
                                    <h3 class="testimonialName">Tristan 
                                        <!-- <span>Lost 25 lbs</span> -->
                                    </h3>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding-bottom:2%">
                                <hr>
                                <div class="col-md-6">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_malissa.jpg" class="img-responsive" alt="Responsive image">
                                    <h3 class="testimonialName">Malissa 
                                        <!-- <span>Lost 35 lbs</span> -->
                                    </h3>
                                </div>
                                <div class="col-md-6">
                                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_mia.jpg" class="img-responsive" alt="Responsive image">
                                    <h3 class="testimonialName">Mia 
                                        <!-- <span>Lost 8 lbs</span> -->
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             <!-- order now stuff -->
            </div>
            <div class="container tryMiddle">
                <div class="col-md-6 col-xs-12">
                    <h3 class="text-center">Ready to Start?</h3>
                </div>
                <div class="col-md-6 col-xs-12 text-center tryMiddle-action">
                    <a href="http://www.maxslim.store/shop">
                        <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/newphen375/try.png" class="tryMiddle-imgTry" alt="Order Now">
                    </a>
                    <div class="security">
                       <!-- <img src="https://dc411ibrlpprl.cloudfront.net/chk.png" class="security-img" alt="Banner Image"> -->
                       <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/phen375/images/seals-security.png">
                       
                    </div>
                </div>
            </div>
         <!-- Women Section -->
            <div class="container testimonial-text" style="padding-top:3%; padding-bottom:1%;">
                <div class="col-md-6">
                    <iframe src="https://player.vimeo.com/video/146969565?title=0&amp;byline=0&amp;portrait=0" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
                <div class="col-md-6">
                   <iframe src="https://player.vimeo.com/video/146969564?title=0&amp;byline=0&amp;portrait=0" width="100%" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            </div>
            <div class="container">
                <div class="col-md-6 col-md-offset-5" style="padding-bottom:1%">
                   <a href="#disclaimer">Disclaimer</a>
                </div>
            </div>
            <div class="container testimonial-text">
            <!--     <div class="col-md-12">
                    <div class="testimonial-text1">
                        <p>When I started using Phen375 I weighed 267 pounds. After 2 months I lost 50 pounds and I have never felt better!
                        These may not be the best pictures, but they are real and you can definitely see the results! At the beginning of my weight loss
                        journey I was not confident enough to take a topless photo but even with these photos you can tell that my face, my legs and my
                        arms are all much thinner and I wouldn't have been able to do it without Phen375!</p>
                        <span>Elysia, USA</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text2">
                        <p>I started with 150 kgs and now after using Phen375 I'm 105 kgs. At first, I thought Phen375 was more talk than action but after
                        using it, my perspective on this marvellous product totally changed. It gave me boost and really suppressed my diet. I continued
                        my workout regime and took Phen375. In 12 weeks I lost 45 kgs</p>
                        <span>Mohammad, Netherlands</span>
                   </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text1">
                        <p>I was 232 in december of 2015 now July I am at 188. I was very out of shape I have tried many diet pills and diet plans, and said
                        enough until I saw this ad for Phen375, I knew I need to try at least one more before heading to doctor for help so I ordered my final pills,
                        and why because they work, I was always out of breath and being a c.n.a. I had a hard time doing my demanding job as heavy as I was,
                        but Phen375 helped with my cravings and helped me out when I was with my kids and they wanted to eat out, thwy curbed my over eating and
                        helped me make better food choices since I was not craving the junk food I have make a change in my mind set and eating more fruits,
                        salads with chicken and cranberries..Healthy eating rocks.. Phen375 help me lose enough weight I bought a bike and now I went from out of
                        breath just walking with my youngest son to riding a bike for 24 miles..it was hard but I did it and without Phen375 I could not be when I am today...
                        there are no caffeine highs or lows but it works and you too will see the pounds come off..even if I missed a dose no big deal just get back on
                        the next day..losing weight is a struggle, and always will be but I know I can reach my goal of 150 with the help of the wonderful pills called phen375.</p>
                        <span>Sarah, USA</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text2">
                        <p>I tried and failed with many other products. I lost 93 lbs with Phen375. My starting weight was 293 lbs and my goal was to get to 200 lbs.
                        I reached my goal in 6 months and have been able to maintain my desired weight through a healthier lifestyle and exercise. Thank you Phen375! </p>
                        <span>Paul, USA</span>
                   </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text1">
                        <p>When I first received the bottles I was very wary and a non-believer. I've tried so many other products before at I had no success.
                        When I read about this product being the number one rated product I decided to try it. After I took the first dose "WOW",
                        it was powerful and effective and I felt my body kick into fat burning mode and I felt much more positive and less depressed.
                        And after continuing for 3 months, with exercise as well, I lost 26kgs. I am proof positive this stuff works. Finally, no more searching.
                        Thanks Phen375 for adding years onto my life and making it a better quality. </p>
                        <span>Ryan, South Korea</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text2">
                        <p>I started to take Phen375 when my weight was 93 kg. After one month I became 89 kg. and after one more month 84 kg. So 9 kg. less after two month.
                        I am very happy and satisfied with this product. Also I used Digest Fast and felt these two months more healthy and happy. So I changed my style and life very much.
                        Thank You so much for what You do.</p>
                        <span>Dylan, Russian Federetion</span>
                   </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text1">
                        <p>Taking Phen375 and paired with diet and exercise has helped me lose 80 pounds and put on 10 pounds of muscle. It curbed my appetite and gave me great energy!
                        I have had zero side effects, and have had nothin but a great experience from using Phen375! So glad I did my research and found this product because it was a life saver.
                        Thanks Phen375!</p>
                        <span>Anna, USA</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text2">
                        <p>Hi I totally recommend this product, I used other products before but this one is amazing help with really small effort. I just fix my eat plan. Due to healthy problem
                        I don't exercise yet. So really helped me I don't even have side effects but attention. I don't have effect because I stick to their instructions don't drink caffeine while
                        I'm on the pill. Also helped small my appetite. Really melts my fat. I use this product for 3 months and I have lost 22 kg. weight before start phen375 and I was 124kg now
                        with this pills I am 102kg.</p>
                        <span>Isabelle, Greece</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text1">
                        <p>I have only been using Phen375 for about 2 1/2 months and have lost about 9 pounds total. When I started I was 183.5 and now am at 174. I plan to keep going!! I am thrilled.
                        I love how Phen375 is not harmful and does not interfere with my other daily routine and still has results. My goal is 160 at least.</p>
                        <span>Taylor, USA</span>
                    </div>
                </div>
                
                <div class="col-md-12">
                    <div class="testimonial-text2">
                        <p>I have been completely astonished at how well Phen375 has worked! I have tried many other diet pills, but none have controlled my appetite like this one. I have successfully lost 21
                            pounds so far! I still have another 5 to lose, but by using this product, I know that it is possible! Sincerely, A very thrilled customer! P.S. I tell everyone about it!</p>
                        <span>Anavel, USA</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text1">
                        <p>I'm very happy with your product, from January I lost 30 lbs. and another 10lbs to go. Thank you for your offer I still have 3 bottles. You guys doing great job. Thank you again</p>
                        <span>Svetlana, Canada</span>
                   </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text2">
                        <p>Thank you for info - have now lost over 28-lbs and am almost at my correct weight of 112lbs. Another 14-lbs to go. Couldn't have done it without help!</p>
                        <span>Joan, United Kingdom</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text1">
                        <p>June 30th, 2012 I weighed in at 324.7 lbs the heaviest I have ever been! As of August 23rd, 2012. I weighed in at 276.4 lbs. Within that time I also didn't take the pills for a whole week because
                        visiting my parents I forgot them!. While on or even off the pills I didn't suffer any side effects or gain any weight back when I wasn't taking them. I stayed the exact same. I still have 12 more
                        days to go and would highly recommend this to anyone looking to loose weight! The only downfall to the whole diet was having to pee extra because your drinking lots of water. That really doesn't
                        matter though because you get to eat 6 meals a day :D! Thanks Phen375!!</p>
                        <span>Tristan, USA</span>
                   </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text2">
                        <p>The first month I lost 18 lbs and I when realized their product worked I stayed with it for the next five months, I have lost about 55 pounds in six months and I feel so much better!</p>
                        <span>Isabella, USA</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text1">
                        <p>Within one month, I have gone from 154 pounds to 139 pounds without following any special diet or exercise routine! This product is amazing and I will continue to purchase it until
                        I reach my pre pregnancy weight of 125. Thank you Phen375 for giving me my body back!</p>
                        <span>Katherine, USA</span>
                   </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text2">
                        <p>Combining Phen375 with a healthy diet and physical exercise I have managed to loose 6.5 stone going from 20 stone to my current 13.5. Overall I am very happy with the product and even
                        though it is expensive the reason for this is because it works.</p>
                        <span>Jessy, Australia</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text1">
                        <p>"I weighed 200 lbs when I started and I now weigh 154 lbs that's 46 lbs GONE for good. From a size 16 to a size 8, saying that actually makes me tear up."</p>
                        <span>Danielle, Canada</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-text2">
                        <p>I met my weight loss goal in time for my wedding day. Thanks Phen375 for making my special day, one to remember. I did everything in the program. I was trying to lose 20 lbs in 10 weeks.
                        I lost 23lbs and fit into my wedding dress with room to spare. Thank you so much.</p>
                        <a name="disclaimer"></a>
                        <span>Elizabeth, USA</span>
                    </div>
                </div> -->
            </div> 
            <div class="container">
                <div class="row-new">
                    <div class="col-md-12"> <!-- -box-2-->
                        <div class="feature-text testimonial">
                            <h1>Testimonial Disclaimer</h1>
                            <p>Testimonials are not claimed to represent typical results. All testimonials are real men and women,
                                and may not reflect the typical user’s experience, and are not intended to represent or guarantee
                                that anyone will achieve the same or similar results. Every person has unique experiences, exercise
                                habits, eating habits, and applies the information in a different way. Thus, the experiences that we
                                share from other people may not reflect the typical users' experience. However, these results are
                                meant as a showcase of what the best, most motivated Phen375 users have achieved. All customers
                                who provided Phen375 their success story were remunerated with free product. On average
                                Phen375 users could expect to lose 5 to 10 pounds per month along with a low calorie diet and low to
                                moderate exercise. You should not begin the program if you have a physical condition that makes
                                intense exercise dangerous. In addition, Phen375 requires you to follow an eating plan and at times
                                restrict the amount of calories you consume. You should not begin this eating plan if you have
                                physical or psychological issues which make fat-loss dangerous. Please consult a physician before
                                beginning any exercise or diet program or if you have a pre-existing health condition that could
                                negatively interact with Phen375’s natural ingredients.</p>
                        </div>
                    </div>
                </div>
            </div>
             <!-- /.feature-text -->
        </div>
          <!-- End Women Section -->
    </div>
      <!-- /.feature-text -->
    <div class="container containerLearnMore">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-4 col-xs-12">
                    <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/phen375/images/phen375_testimonial_disclaimer.png" class="img-responsive centerElementBlock" alt="Disclaimer Image">
                </div>
                <div class="col-md-8 col-xs-12">
                    <p class="containerLearnMore-text">
                        <strong>Phen375</strong> Testimonial Giveaway! Share your Phen375 success story testimonial and we'll give you free supply of <strong>Phen375</strong>.
                        Learn how you can apply.
                    </p>
                    <div class="text-center btn-container">
                        <a href="testimonials-guide.html" class="containerLearnMore-btn colorBlueDefault">Learn More</a>                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="container">   
  <div class="row"> <div class="row last-banner">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <img src="http://www.maxslim.store/wp-content/uploads/2018/05/blot.png" class="img-responsive center-block" alt="Banner Image">
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h2 class="blue font40">
                    <span class="special-font extra-bold block-span">START YOUR JOURNEY TODAY! TRY PHEN375!</span>
                </h2>
                <h3 class="bold">No Prescription Required!</h3>
                <a href="http://www.maxslim.store/shop" class="button button-links">Rush My Order</a>
                <div style="margin-top: 30px;">
                    <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                        <img name="trustseal" alt="Security Seals" style="border: 0; width: 120px;" src="<?php echo get_theme_file_uri(); ?>/assets/def/img/6121-lg.gif" />
                    </a>
                </div>

                <span class="block-span">
                                      </span>
            </div>
           <!--  <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padd">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/fad.png" class="img-responsive seal-img" alt="Made in a FDA Approved Facility">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in a FDA</span>
                        <span class="dark-blue block-span special-font bold">Approved Facility</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img2.png" class="img-responsive seal-img" alt="100% Quality Guaranteed">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">100% Quality</span>
                        <span class="dark-blue block-span special-font bold">Guaranteed</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img3.png" class="img-responsive seal-img" alt="Made in USA">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in</span>
                        <span class="dark-blue block-span special-font bold">USA</span>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4 col-xs-12">
            <div class="row">
                <!-- <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-6 seals">
                                            <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
                            <tr>
                                <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
                                </td>
                            </tr>
                        </table>
                                    </div> -->

                <!-- <div class="col-md-12 col-sm-12 col-xs-1 seals">
                    <center>
                        <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                            <img name="trustseal" alt="Security Seals" style="border: 0; width: 160px;" src="//dw26xg4lubooo.cloudfront.net/seals/stacked/6121-lg.gif" />
                        </a>
                    </center>
                </div> -->
            </div>
        </div>
</div></div>        
</div>
</div>						<!-- End Content -->

<?php get_footer( 'home' ); ?>