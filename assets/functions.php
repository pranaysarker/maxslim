<?php

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function maxslim_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/maxslim
	 * If you're building a theme based on maxslim, use a find and replace
	 * to change 'maxslim' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'maxslim' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	// making theme woocommerce supported
	add_theme_support( 'woocommerce' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'maxslim-featured-image', 2000, 1200, true );

	add_image_size( 'maxslim-thumbnail-avatar', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'maxslim' ),
        'social' => __( 'Social Links Menu', 'maxslim' ),
        'main-menu' => __( 'Main Menu', 'maxslim' ),
        'main-menu-copy' => __( 'Main Menu Copy', 'maxslim' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	/**
	 * Filters maxslim array of starter content.
	 *
	 * @since maxslim 1.1
	 *
	 * @param array $starter_content Array of starter content.
	 */

}
add_action( 'after_setup_theme', 'maxslim_setup' );


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function maxslim_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'maxslim' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'maxslim' ),
		'before_widget' => '<div class="col-sm-3 col-6">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'maxslim' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'maxslim' ),
		'before_widget' => '<div class="col-sm-3 col-6">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'maxslim' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your footer.', 'maxslim' ),
		'before_widget' => '<div class="col-sm-3 col-6">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer 3', 'maxslim' ),
		'id'            => 'sidebar-4',
		'description'   => __( 'Add widgets here to appear in your footer.', 'maxslim' ),
		'before_widget' => '<div class="col-sm-3 col-6">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 4', 'maxslim' ),
		'id'            => 'sidebar-5',
		'description'   => __( 'Add widgets here to appear in your footer.', 'maxslim' ),
		'before_widget' => '<div class="col-sm-3 col-6">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 5 Big', 'maxslim' ),
		'id'            => 'sidebar-6',
		'description'   => __( 'Add widgets here to appear in your footer.', 'maxslim' ),
		'before_widget' => '<div class="col-md-4">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="bold">',
		'after_title'   => '</h5>',
	) );



	register_sidebar( array(
		'name'          => __( 'Woocommerce Sidebar', 'maxslim' ),
		'id'            => 'woocommerce-sidbar',
		'description'   => __( 'Add widgets here to appear woocommerce.', 'maxslim' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="bold">',
		'after_title'   => '</h5>',
	) );
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar copy', 'maxslim' ),
		'id'            => 'sidebar-7',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'maxslim' ),
		'before_widget' => '<div class="col-md-3 col-sm-3">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1 copy', 'maxslim' ),
		'id'            => 'sidebar-8',
		'description'   => __( 'Add widgets here to appear in your footer.', 'maxslim' ),
		'before_widget' => '<div class="col-md-3 col-sm-3">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2 copy', 'maxslim' ),
		'id'            => 'sidebar-9',
		'description'   => __( 'Add widgets here to appear in your footer.', 'maxslim' ),
		'before_widget' => '<div class="col-md-3 col-sm-3">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer 3 copy', 'maxslim' ),
		'id'            => 'sidebar-10',
		'description'   => __( 'Add widgets here to appear in your footer.', 'maxslim' ),
		'before_widget' => '<div class="col-md-3 col-sm-3">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 4 copy', 'maxslim' ),
		'id'            => 'sidebar-11',
		'description'   => __( 'Add widgets here to appear in your footer.', 'maxslim' ),
		'before_widget' => '<div class="col-md-3 col-sm-3">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 5 Big copy', 'maxslim' ),
		'id'            => 'sidebar-12',
		'description'   => __( 'Add widgets here to appear in your footer.', 'maxslim' ),
		'before_widget' => '<div class="col-md-4">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="bold">',
		'after_title'   => '</h5>',
	) );



	register_sidebar( array(
		'name'          => __( 'Woocommerce Sidebar copy', 'maxslim' ),
		'id'            => 'woocommerce-sidbar-copy',
		'description'   => __( 'Add widgets here to appear woocommerce.', 'maxslim' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="bold">',
		'after_title'   => '</h5>',
	) );
}
add_action( 'widgets_init', 'maxslim_widgets_init' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since maxslim 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function maxslim_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'maxslim' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'maxslim_excerpt_more' );




/**
 * Enqueue scripts and styles.
 */
function maxslim_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'awesome', get_theme_file_uri('assets/css/font-awesome.min.css') );
	wp_enqueue_style( 'bootstrap', get_theme_file_uri('assets/css/bootstrap.min.css') );
	wp_enqueue_style( 'slicknav', get_theme_file_uri('assets/css/slicknav.min.css') );
    wp_enqueue_style( 'theme', get_theme_file_uri('assets/css/theme.css') );
    wp_enqueue_style('stylesheet', get_stylesheet_uri() );
	wp_enqueue_style( 'responsive', get_theme_file_uri('assets/css/responsive.css') );



	wp_enqueue_script( 'jqueryy', get_theme_file_uri( 'assets/js/jquery-3.3.1.min.js' ), array(), '1.0', true );
	wp_enqueue_script( 'bootstrap', get_theme_file_uri( 'assets/js/bootstrap.min.js' ), array('jqueryy'), '1.0', true );
	wp_enqueue_script( 'slicknav', get_theme_file_uri( 'assets/js/jquery.slicknav.min.js' ), array('jqueryy'), '1.0', true );
	wp_enqueue_script( 'main', get_theme_file_uri( 'assets/js/main.js' ), array('jqueryy'), '1.0', true );


}
add_action( 'wp_enqueue_scripts', 'maxslim_scripts' );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since maxslim 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function maxslim_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'maxslim_content_image_sizes_attr', 10, 2 );

/**
 * Filter the `sizes` value in the header image markup.
 *
 * @since maxslim 1.0
 *
 * @param string $html   The HTML image tag markup being filtered.
 * @param object $header The custom header object returned by 'get_custom_header()'.
 * @param array  $attr   Array of the attributes for the image tag.
 * @return string The filtered header image HTML.
 */
function maxslim_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'maxslim_header_image_tag', 10, 3 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since maxslim 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function maxslim_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( is_archive() || is_search() || is_home() ) {
		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
	} else {
		$attr['sizes'] = '100vw';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'maxslim_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since maxslim 1.0
 *
 * @param string $template front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function maxslim_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template',  'maxslim_front_page_template' );

require_once get_template_directory() .'/lib/codestar/cs-framework.php';


// woocommerce function work
require_once('inc/woocommerce-func.php');


