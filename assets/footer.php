<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="row">
						<?php dynamic_sidebar(2); ?>
						<?php dynamic_sidebar(3); ?>
						<?php dynamic_sidebar(4); ?>
						<?php dynamic_sidebar(5); ?>
						<!-- <div class="social-links">
							<a href="#" target="_blank"><img src="https://dc411ibrlpprl.cloudfront.net/icon-1.png"></a>
							<a href="#" target="_blank"><img src="https://dc411ibrlpprl.cloudfront.net/icon-2.png"></a>
							<a href="#" target="_blank"><img src="https://dc411ibrlpprl.cloudfront.net/icon-5.png"></a>
							<a href="#" target="_blank"><img src="https://dc411ibrlpprl.cloudfront.net/icon-4.png"></a>
							<a href="#" target="_blank"><img src="https://dc411ibrlpprl.cloudfront.net/icon-6.png"></a>
						</div> -->

					</div>
				</div>
                <span class="footer-border"></span>
                <?php dynamic_sidebar(6); ?>
				<!-- <div class="col-md-4">
					<h5 class="bold">Ready for more weight-loss tips? <br> Get our free e-book: </h5>
					<form action="index.html" target="_blank" novalidate="">
						<input class="text-input" type="text" placeholder="Name" name="FNAME" required="">
						<input class="text-input" type="email" placeholder="Email" name="EMAIL" required="">
						<input id="btnCheckFreeEBook" class="theme-btn button buttonlinks" type="submit" value="Get Your Copy!">
					</form>
				</div> -->
				<div class="col-md-12 margin-top-40">
					<div class="row">
						<div class="col-md-7">
							<div class="footer-bottom-text">							
								<?php if( !empty( cs_get_option('footer_bottom_one') ) ){
                                    echo cs_get_option('footer_bottom_one');
                                } ?>	
							</div>
						</div>
						<div class="col-md-4 offset-md-1">
							<div class="footer-copy-right-text">							
								<?php if( !empty( cs_get_option('footer_bottom_two') ) ){
                                    echo cs_get_option('footer_bottom_two');
                                } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	

    <?php wp_footer(); ?>
</body>
</html>