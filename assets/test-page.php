<?php /* Template Name: Test Page */ 
	get_header(  );
?>
<div class="container">
	<div class="row">
		<div class="col-md-8">
			
			<div class="products">
				<div class="product-container">
				<div class="product">
					<div class="product-top">
						<div class="title">
							<h1>BEST DEAL! BUY 4 BOTTLES GET 1 FREE!</h1>
						</div>
						<div class="best-value">
							<img src="<?php echo get_theme_file_uri( 'assets/images/best-value.png' ); ?>" alt="">
						</div>
					</div>
					<div class="product-content row">
						<div class="product-image saving col-md-8">
							<img src="http://localhost/sajid-vai/wp-content/uploads/2018/05/four_bottles_plus_one_free__.png" alt="">
							<div class="totla-saving text-center">
								<span class="d-block bold text-small">Total</span>
								<span class="d-block bold text-small">Savings</span>
								<span class="d-block bold">$261.99</span>
							</div>
						</div>
						<div class="product-price col-md-4 text-right pt-4 pr-4">
							
                                <span class="d-block regular-tagline bold text-uppercase">Regular Price</span>
                                <span class="d-block regular-price bold" style="text-decoration: line-through; font-size: 25px;">$449.95</span>
                                <h3 class="sels-price bold">$187.96</h3>
                                <p class="hands">+S&amp;H $ 19.95</p>

                                <div class="select right"><a href="/en/checkout/1.html" class="pc-ac"><img style="width:auto;height: auto;" src="<?php echo get_theme_file_uri( 'assets/images/select.png' ); ?>" alt="Select"></a>                                </div>
                                <div class="clear"></div>
                            
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<h1>Sidebar Area</h1>
		</div>
	</div>
</div>


<?php get_footer(); ?>
