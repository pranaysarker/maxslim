<?php get_header(); ?>

	<div class="container">
		<div class="hero-area">
			<div class="row">
				<div class="col-md-3  col-sm-6">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/phen375_home_mbgc.png" alt="">
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="hero-content">
			            <h1 class="blue">
			                <span class="text-1st blue margin-top-20 extra-bold">Lose Weight</span>
			                <span class="text-2nd">Faster &amp; Easier</span>
			                <span class="text-3rd">Than Ever Before</span>
			            </h1>
			            <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/border.png" class="img-responsive" alt="border green">
			            <p class="special-font semi-bold">Improving your metabolism and suppressing your hunger will increase your body's ability to burn fat!</p>
			            <ul class="features special-font">
			                <li><img src="https://dc411ibrlpprl.cloudfront.net/newphen375/scale.png" class="img-responsive" alt="scale"> <span>Highly refined ingredients!</span></li>
			                <li><img src="https://dc411ibrlpprl.cloudfront.net/newphen375/apple.png" class="img-responsive" alt="apple"> <span>May Reduce Food Cravings!</span></li>
			                <li><img src="https://dc411ibrlpprl.cloudfront.net/newphen375/tape.png" class="img-responsive" alt="tape"> <span>Increasing Metabolism can Burn Fat!</span></li>
			            </ul>
					</div>
				</div>
				<div class="col-md-5">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/img5.png" alt="">
				</div>
			</div>
		</div>
		<div class="call-to-action-area">
			<div class="row">
				<div class="col-md-7">
					<h2 class="">Burning fat daily is possible with an increased metabolism!</h2>
            		<span class="">Get started Now!</span>
				</div>
				<div class="col-md-5">
					<a class="theme-btn">Order Your Supply Today!</a>
				</div>
			</div>
		</div>
		<div class="section-area">
			<div class="row">
				<div class="col-md-7">
					<div class="content">
						<h3 class="theme-heading margin-top-40">Become a Slimmer, More Sexy You!</h3>
						<p> Imagine yourself becoming slimmer and getting in shape! Losing the extra pounds may not only make you feel better, but also may, help you look better. </p>
						<p class="bold italic font-20"> Phen375 is a dietary supplement that may lead to weight and hunger suppression! </p>
						<p>Consuming excessive calories without burning it off normally results in weight gain. Phen375 has specifically-designed diet plans and exercise routines created to help you burn fat.</p>
					</div>
				</div>
				<div class="col-md-5">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/phen375_woman_bottlesc.jpg" alt="">
				</div>
			</div>
		</div>
		<div class="section-area">
			<div class="row">
				<div class="col-md-12">
					<h3 class="theme-heading text-center">Suppress Cravings, Burn Fat &amp; Lose Weight</h3>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="single-item">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/scale2.png" alt="">
						<p class="italic">Calorie Reduction, Leads To Weight Loss</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="single-item">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/scale2.png" alt="">
						<p class="italic">Calorie Reduction, Leads To Weight Loss</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="single-item">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/scale2.png" alt="">
						<p class="italic">Calorie Reduction, Leads To Weight Loss</p>
					</div>
				</div>
				<div class="col-md-12 text-center">
					<a href="#" class="theme-btn text-center margin-top-20">Try Phen375 Today!</a>
				</div>
			</div>
		</div>
		<div class="pricing-area margin-top-30">
			<div class="row text-center">
				<div class="col-md bg-orange">
					<div class="content">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/checknorton_42px.png" alt="">
						<h4>Norton</h4>
						<span>tm</span>
					</div>
				</div>
				<div class="col-md bg-black">
					<span class="big-font">SHOPPING GUARANTEE</span>
				</div>
				<div class="col-md bg-gray">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-4.png" alt="">
					<span class="rate">$1000</span>
					<p>ID theft Protection</p>
				</div>
				<div class="col-md bg-gray">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-3.png" alt="">
					<span class="rate">$1000</span>
					<p>Purchase Guarantee</p>
				</div>
				<div class="col-md bg-gray">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-4.png" alt="">
					<span class="rate">$1000</span>
					<p>Lowest Price Guarantee</p>
				</div>
			</div>
		</div>
		<div class="count-area">
			<div class="row">
				<div class="col-sm-6">
		            <div class="count_img">
		                <img src="https://dc411ibrlpprl.cloudfront.net/homepage-v2/icon-1.png" class="img-responsive">
		            </div>
		            <div class="count_number">
		                <span>152.8</span>
		            </div>
		            <div class="count_text">
		                <p>Million</p>
		                <p>Unique Visitors</p>
		            </div>
				</div>
				<div class="col-sm-6">
		            <div class="count_img">
		                <img src="https://dc411ibrlpprl.cloudfront.net/homepage-v2/icon-1.png" class="img-responsive">
		            </div>
		            <div class="count_number">
		                <span>152.8</span>
		            </div>
		            <div class="count_text">
		                <p>Million</p>
		                <p>Unique Visitors</p>
		            </div>
				</div>
			</div>
		</div>
		<div class="testimonial-area margin-top-20">
			<div class="row">
				<div class="col-md-12 text-center">
					<h3 class="theme-heading">Become a Slimmer, Sexier More Attractive You!</h3>
					<p class="sub-title bold">Imagine yourself becoming slimmer and getting in shape! Losing the extra pounds may not only make you feel better but also may, help you look better.</p>
				</div>
				<div class="col-md-4">
					<div class="single-testimonial">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimony-image-1.jpg" alt="">
						<span class="position">Catherine, USA</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="single-testimonial">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimony-image-2.jpg" alt="">
						<span class="position">Denise, USA</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="single-testimonial">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/testimony-image-3.jpg" alt="">
						<span class="position">Evelyn, USA</span>
					</div>
				</div>
				<div class="col-md-12 text-center">
					<a href="#" class="theme-btn text-center margin-top-20">Order Your Supply Today!</a>
				</div>
			</div>
		</div>
		<div class="order-count-area">
			<div class="row">
				<div class="col-md-6">
					<h2 class="theme-heading">CUSTOMERS BUYING <span>RIGHT NOW! TRY PHEN375!</span></h2>
				</div>
				<div class="col-md-6">
					<div class="order-count">
						<ul>
							<li>1</li>
							<li>2</li>
							<li>3</li>
							<li>4</li>
							<li>5</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="section-area">
			<div class="row">
				<div class="col-md-4 service-area">
					<div class="single-service">
						<div class="img-div">
							<img src="https://dc411ibrlpprl.cloudfront.net/newphen375/1.png" class="img-responsive center-block" alt="icon">
						</div>
						<div class="content">
							<span class="extra-bold">Calorie reduction!</span>
							<span>Leads to weight loss</span>
						</div>
					</div>
					<div class="single-service">
						<div class="img-div">
							<img src="https://dc411ibrlpprl.cloudfront.net/newphen375/1.png" class="img-responsive center-block" alt="icon">
						</div>
						<div class="content">
							<span class="extra-bold">Calorie reduction!</span>
							<span>Leads to weight loss</span>
						</div>
					</div>
					<div class="single-service">
						<div class="img-div">
							<img src="https://dc411ibrlpprl.cloudfront.net/newphen375/1.png" class="img-responsive center-block" alt="icon">
						</div>
						<div class="content">
							<span class="extra-bold">Calorie reduction!</span>
							<span>Leads to weight loss</span>
						</div>
					</div>
				</div>
				<div class="col-md-8 service-detail">
					<h3 class="theme-heading">You Can Change Your Life Now!</h3>
					<p>Doctor's, dieticians and other experts agree that the best way to lose weight is to eat fewer calories and be more active</p>
					<p>For most people, a reasonable goal is to lose about a pound a week, which means, cutting about 500 calories a day from your diet, eating a variety of nutritious foods and exercising regularly!</p>

					<p class="bold italic font-20">Let Phen375 assist you in your weight-loss journey!</p>
					<p>
					If you buy now you can take advantage of the free diet plan and cellulite reduction guide being offered for a limited time when you purchase Phen375.
					</p>
				</div>

				<div class="section-area margin-top-40">
					<div class="row">
					    <div class="col-md-4 text-center">
					        <img class="width70" src="https://dc411ibrlpprl.cloudfront.net/phen375/images/2bottle-sealc.png" class="img-responsive center-block" alt="Banner Image">
					    </div>
					    <div class="col-md-8">
					        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
					            <h2 class="theme-heading margin-top-20 font40">START YOUR JOURNEY TODAY! TRY PHEN375!</h2>
					            <h3 class="bold margin-top-30 italic">No Prescription Required!</h3>
					            <a href="#" class="theme-btn margin-top-10">Rush My Order</a>
					            <a class="block margin-top-30" href="#">
					            	<img class="margin-bottom-10 width-100" src="https://dw26xg4lubooo.cloudfront.net/seals/stacked/6121-lg.gif">
					            </a>
					            
					        </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>