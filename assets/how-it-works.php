<?php /* Template Name: HowItWorks */
get_header('home');
?>
<div class="container">
										<div class="company-logo-top">
												<img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/company-logo-top.jpg" alt="Matrix" id="tag" />										</div>

										
										<div class="visible-sm-12 menu-phone top-link">
												<div class="col-sm-12">
														<span><a href="track-my-order.html">Track My Order</a></span> |
														<span><a href="contact-us.html">Contact Us</a></span>
												</div>
												<div class="clearfix center-block select-container">
														<div class="pull-left">
																																<form  method="post">
																		<select id="currchangePhone" name="ChangeCurrency" onchange="this.form.submit()">
																				<option value="USD" selected='selected'>USD</option>
																				<option value="EUR" >EUR</option>
																				<option value="GBP" >GBP</option>
																				<option value="AUD" >AUD</option>
																				<option value="MXN" >MXN</option>
																				<option value="CAD" >CAD</option>
																		</select>
																</form>
														</div>
														<div class="pull-left">
																<select id="changLangPhone">
																		<option value='https://www.phen375.com/en/appetite-suppression.html' selected='selected'>English</option>
																		<option value='https://www.phen375.com/fr/appetite-suppression.html' >Francais</option>
																<!--Nestrix | Ing. Franco Salas-->
																		<!--Set Deutsch Language-->
																		<option value='https://www.phen375.com/de/appetite-suppression.html' >Deutsch</option>
																		<!--Set Greek Language-->
																		<option value='https://www.phen375.com/el/appetite-suppression.html' >Ελληνική</option>
																		<!--Set Spanish Language-->
																		<option value='https://www.phen375.com/es/appetite-suppression.html' >Español</option>
																		<!--Set Italian Language-->
																		<option value='https://www.phen375.com/it/appetite-suppression.html' >Italiano</option>
																		<!--Set Dutch Language-->
																		<option value='https://www.phen375.com/nl/appetite-suppression.html' >Dutch</option>
																<!--End Modifications-->
																</select>
														</div>
												</div>
										</div>

										<div class="containerMenuMobile overContentRelative">
											
																						<div id="menuMobile" class="overContent"></div>

											<!-- <div class="fullWidth fullHeight overContentRelative">
												<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
												</div>
											</div> -->
											<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
																									<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
														<tr>
															<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
															</td>
														</tr>
													</table>
																							</div>
											<a href="http://www.maxslim.store/shop" class="button inlineBlock overContent overContent-topRight containerMenuMobile-btnOrderNow">Order Now</a>										</div>

										
								</div>
						</div>
						<!-- End Header -->

						<!-- Content -->
						
						<link rel="canonical" href="appetite-suppression.html"/>
<div class="wrapper">
   <!-- content -->
   <div class="container content">
      <div class="row">
         <div class="col-md-7 col-sm-7">
            <div class="feature-text box-1">
               <h1>How It Works</h1>
               <ul>
                  <!-- <li>Effective appetite control</li> -->
<!--                   <li>Increase metabolism</li>
                  <li>Appropriate diet plan provided</li>
                  <li>Increased water intake, clears toxins</li>
                  <li>Increasing exercise, increases muscle tissue</li>
                  <li>Physical exercise / home exercise videos provided</li> -->
                  <li>May increase metabolism</li>
                  <li>Diet plans provided</li>
                  <li>Increased water intake, may clear toxins </li>
                  <li>Increasing exercise, may increase muscle tissue </li>
                  <li>Home exercise videos provided </li>
               </ul>
            </div>
         </div>
         <div class="col-md-5 col-sm-5">
            <div class="feature-text row-img">
               <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/phen375/images/phen375_pills_.png.png"
                    class="img-responsive" alt="Phen375 Bottles">
            </div>
         </div>
      </div>
         <!-- /.feature-text -->
   </div>
   <div class="container">
      <div class="row">
         <div class="col-md-5 col-sm-5">
            <div class="feature-text box center">
               <!-- <h1>Maximum Strength</h1><h1>without the Prescription!</h1> -->
               <!-- <span>without the Prescription! Order Now</span><br> -->
               <a href="https://www.phen375.com/phen375/home/phen375" class="button buttonlinks">Order Today!</a>
              <!--  <p>Join over 222,955  customers and start your weight loss journey!</p> -->
            </div>

            <div class="feature-text row-img">
               <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/appetite_page_mesure.jpg">
            </div>
            <div class="text-center" style="margin-top:35px;">
               <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/phen375/images/Seal.png" align="Money Back ">   
            </div>         
         </div>

         <div class="col-md-7 col-sm-7">
            <div class="feature-text">
               <h1>Start your weight-loss journey with Phen375, Today!</h1>
               <p>
                  <!-- Phen375 has been designed to help suppress appetite as well as increase the body's metabolic rate. The most common reason that dieting is so difficult is due to the inability to control appetite and maintain an optimal metabolic rate. -->
                  Phen375 is manufactured in an FDA-registered facility, which holds current cGMP/NSF certificate. This ensures FDA-required testing, such as microbial and heavy metal testing before releasing Phen375 into the marketplace. 
               </p>

               <p>
                  <!-- Appetite control is essential to long term weight loss. Meals should never be skipped, rather, they should be controlled. Skipping meals actually forces your body to go into a starvation mode. It slows down the metabolic rate to preserve itself and makes the fat burning process more difficult. Ideally, you want to reduce calories, while also increasing meal frequency. <strong>Phen375 was designed to aid you in controlling your appetite and along with our diet plans the process becomes easier to accomplish</strong>.  -->
                  The most common reason that dieting is so difficult for some, is due to the inability to control appetite and maintain an optimal metabolic rate. Appetite control is essential to long-term weight loss. Skipping meals may actually force your body to go into starvation mode. It could slow down the metabolic rate to preserve itself and potentially make the fat-burning process more difficult.
                  <strong>Phen375 diet plans were designed to aid you in controlling your appetite, which should make the process easier to accomplish.</strong>
               </p>

               <p>
                  <!-- Rapid weight loss is accomplished or occurs when your body requires calories that are not present in the body. The body has to break fat down into energy. However, fat is where the body stores toxins. Once the fat burning process begins, it also releases toxins.  -->
                  Weight loss can be accomplished or occurs when your body requires calories that are not present in the body. The body has to break fat down into energy. However, fat is where the body often stores toxins. Once the fat-burning process begins, toxins may be released. 
               </p>

               <p>
                  <!-- The best way to flush these toxins is to consume a healthy amount of water. Phen375 is designed to naturally help increase thirst which obviously leads to an increase in water consumption. -->
                  The best way to flush these toxins is to consume a healthy amount of water. 
               </p>
            </div>
         </div>

      </div>
      <!-- /.feature-text -->
   </div>


   <div class="container">
      <div class="row">
         <div class="col-md-6">
            <h2>Raymond</h2>
            <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_raymond.jpg" alt="Male testimonial" class="img-responsive">
         </div>
         <div class="col-md-6">
            <h2>Danielle</h2>
            <img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/testimonials/testimonial_danielle.jpg" alt="Female testimonial" class="img-responsive">
         </div>
      </div>
   </div>

   <div class="spacer"></div>
   <div class="container">
      <div class="feature-text col-md-12 center"> <!--box-gray  -->
         <h1 class="program">
            <!-- Phen375 is more than just a pill - it's a weight loss success program! -->
            Phen375 is more than just a pill, when you combine it with diet and exercise!               
         </h1>
         <div class="col-md-12">
            <div class="col-md-3">
               <div class="box-1 center box-1_custom">
                  <h3>
                     <!-- Drink more water, lose weight! <br> &nbsp; -->
                     Drink More Water! 
                  </h3>
               </div>
            </div>
            <div class="col-md-3">
               <div class="box-1 center box-1_custom">
                  <h3>
                     <!-- Increase your metabolism to burn fat quicker! -->
                        Increase Your Metabolism! 
                  </h3>
               </div>
            </div>
            <div class="col-md-3">
               <div class="box-1 center box-1_custom">
                  <h3>
                     <!-- Physical Activity stimulates muscle tissue & prevents muscle loss! -->
                        Increase Physical Activity! 
                  </h3>
               </div>
            </div>
            <div class="col-md-3">
               <div class="box-1 center box-1_custom">
                  <h3>Burn more calories, lose weight!</h3>
                  <!-- Burn More Calories! -->
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="container">   
      <div class="row"> <div class="row last-banner">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <img src="http://www.maxslim.store/wp-content/uploads/2018/05/blot.png" class="img-responsive center-block" alt="Banner Image">
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h2 class="blue font40">
                    <span class="special-font extra-bold block-span">START YOUR JOURNEY TODAY! TRY PHEN375!</span>
                </h2>
                <h3 class="bold">No Prescription Required!</h3>
                <a href="http://www.maxslim.store/shop" class="button button-links">Rush My Order</a>
                <div style="margin-top: 30px;">
                    <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                        <img name="trustseal" alt="Security Seals" style="border: 0; width: 120px;" src="<?php echo get_theme_file_uri(); ?>/assets/def/img/6121-lg.gif" />
                    </a>
                </div>

                <span class="block-span">
                                      </span>
            </div>
           <!--  <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padd">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/fad.png" class="img-responsive seal-img" alt="Made in a FDA Approved Facility">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in a FDA</span>
                        <span class="dark-blue block-span special-font bold">Approved Facility</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img2.png" class="img-responsive seal-img" alt="100% Quality Guaranteed">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">100% Quality</span>
                        <span class="dark-blue block-span special-font bold">Guaranteed</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img3.png" class="img-responsive seal-img" alt="Made in USA">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in</span>
                        <span class="dark-blue block-span special-font bold">USA</span>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4 col-xs-12">
            <div class="row">
                <!-- <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-6 seals">
                                            <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
                            <tr>
                                <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
                                </td>
                            </tr>
                        </table>
                                    </div> -->

                <!-- <div class="col-md-12 col-sm-12 col-xs-1 seals">
                    <center>
                        <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                            <img name="trustseal" alt="Security Seals" style="border: 0; width: 160px;" src="//dw26xg4lubooo.cloudfront.net/seals/stacked/6121-lg.gif" />
                        </a>
                    </center>
                </div> -->
            </div>
        </div>
</div></div>        
    </div>
</div>						<!-- End Content -->
<?php get_footer('home'); ?>