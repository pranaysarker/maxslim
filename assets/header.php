<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <?php wp_head(); ?>
</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="logo">
						<?php the_custom_logo(); ?>
					</div>
				</div>
				<div class="col-sm-8">
					<div class="row">
						<div class="col-sm-12">
							<div class="header-top-link">
                                <?php wp_nav_menu(array(
                                    'theme_location'    => 'top',
                                    'container'         => '',
                                    'menu_class'        => '',
                                    'menu_id'           => ' '
                                )); ?>
							</div>
                        </div>
                        <?php
                            if( NULL != cs_get_option('header_list_icon') ){
                                foreach ( cs_get_option('header_list_icon') as $key ) {
                                    ?>
                                    
                            <div class="col-md-3 col-6">
                                <div class="header-bottom-link">
                                    <?php
                                     if( $key['header_icon_select'] == 'icon' ) {
                                        echo '<i class="'.$key["header_icon"].'"></i>';
                                     }elseif( $key['header_icon_select'] == 'image' ) {
                                        $imgLink = wp_get_attachment_image_src( $key['header_image'], 'thumbnail',  true )[0];
                                        echo sprintf('<img src="%s" alt="">',$imgLink);
                                     }
                                     if( !empty( $key["header_text"] ) ){
                                         echo sprintf('<span>%s</span><br>',$key['header_text']);
                                     }
                                     if( !empty( $key["header_text_second"] ) ){
                                         echo sprintf('<span>%s</span>',$key['header_text_second']);
                                     }
                                     ?>
                                </div>
                            </div>
                                     <?php
                                }
                            }
                        ?>
					</div>
				</div>
				<div class="col-md-12">
                    <?php 
                        wp_nav_menu(array(
                            'theme_location'    => 'main-menu',
                            'container_class'   => 'menu',
                            'menu_class'        => ''
                        ));
                    ?>
				</div>
			</div>
		</div>
	</header>
