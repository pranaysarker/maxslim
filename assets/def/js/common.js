var common = {};
(function(){
    'use strict';
    //Private variable
    var that = this;
    this.limitScroll = 170;
    this.navigatorSayswho = {};

    this.init = function(){
        this.saveEmail();
    }

    this.saveEmail = function(){
        var email = this.getParamater("om_email");
        if(email){
            this.getSaveEmailAjax(email);
        }
    };

    //var param1var = getQueryVariable("param1");

    this.getParamater = function(variable){
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
            return pair[1];
        }
      }
      return false;
    }

    this.getSaveEmailAjax = function(email){
        var url = globalValues.baseUrlWebRoot + "home/"
        return $.ajax({
            method: "POST",
            data: {email: email},
            url: url + "saveEmail"
        });
    };


    this.readyJ = function(){
        $(window).load(that.windowLoad);
    };

    this.windowLoad = function(){
        that.init();
    };

    this.navigatorSayswho = (function(){
        var ua= navigator.userAgent, tem,
        M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
        }
        if(M[1]=== 'Chrome'){
            tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
            if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        }
        M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
        return M.join(' ');
    })();


    $(document).ready(this.readyJ);

    return this;
}).call(common);