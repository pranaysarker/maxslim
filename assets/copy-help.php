<?php /* Template Name: FAQ Help */ 
get_header( 'home' );
 ?>
<div class="container">
										<div class="company-logo-top">
												<img src="<?php echo get_theme_file_uri(); ?>/assets/def/img/company-logo-top.jpg" alt="Matrix" id="tag" />										</div>

										
										<div class="visible-sm-12 menu-phone top-link">
												<div class="col-sm-12">
														<span><a href="track-my-order.html">Track My Order</a></span> |
														<span><a href="contact-us.html">Contact Us</a></span>
												</div>
												<div class="clearfix center-block select-container">
														<div class="pull-left">
																																<form  method="post">
																		<select id="currchangePhone" name="ChangeCurrency" onchange="this.form.submit()">
																				<option value="USD" selected='selected'>USD</option>
																				<option value="EUR" >EUR</option>
																				<option value="GBP" >GBP</option>
																				<option value="AUD" >AUD</option>
																				<option value="MXN" >MXN</option>
																				<option value="CAD" >CAD</option>
																		</select>
																</form>
														</div>
														<div class="pull-left">
																<select id="changLangPhone">
																		<option value='https://www.phen375.com/en/help.html' selected='selected'>English</option>
																		<option value='https://www.phen375.com/fr/help.html' >Francais</option>
																<!--Nestrix | Ing. Franco Salas-->
																		<!--Set Deutsch Language-->
																		<option value='https://www.phen375.com/de/help.html' >Deutsch</option>
																		<!--Set Greek Language-->
																		<option value='https://www.phen375.com/el/help.html' >Ελληνική</option>
																		<!--Set Spanish Language-->
																		<option value='https://www.phen375.com/es/help.html' >Español</option>
																		<!--Set Italian Language-->
																		<option value='https://www.phen375.com/it/help.html' >Italiano</option>
																		<!--Set Dutch Language-->
																		<option value='https://www.phen375.com/nl/help.html' >Dutch</option>
																<!--End Modifications-->
																</select>
														</div>
												</div>
										</div>

										<div class="containerMenuMobile overContentRelative">
											
																						<div id="menuMobile" class="overContent"></div>

											<!-- <div class="fullWidth fullHeight overContentRelative">
												<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
												</div>
											</div> -->
											<div class="containerMenuMobile-iconNorton overContent overContent-topCenter">
																									<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
														<tr>
															<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
															</td>
														</tr>
													</table>
																							</div>
											<a href="http://www.maxslim.store/shop" class="button inlineBlock overContent overContent-topRight containerMenuMobile-btnOrderNow">Order Now</a>										</div>

										
								</div>
						</div>
						<!-- End Header -->

						<!-- Content -->
						
						      <link rel="canonical" href="help.html" />
      <div class="wrapper">
         <!-- content -->

         <div class="container content">
            <div class="row">
               <div class="col-md-12 feature-text faq-page">
                  <h1>FAQ (Questions and Answers)</h1>
                  <h4>Order status </h4>
                  <div class="panel-group" id="accordion">
                     <div class="panel panel-default">
                        <div class="panel-heading"  role="tab" id="headingOne">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">• Has my order shipped ?</a>
                           </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                           <div class="panel-body">
                              <p>Typically orders are sent to shipping for dispatch at 11 a.m. each business day. If your order does not ship the same day you placed it due to USPS pickup cut-off times, your package will ship the next business day. For example, if you order at 1:30 p.m. on a Friday, and the following Monday is a holiday, then the next shipping day would be Tuesday.</p>

                              <p>All orders are shipped using www.usps.com services, priority mail and priority international mail. If your shipping address is within three states of the Texas warehouse, we use first class mail, which has the same delivery time as priority mail. All shipments have tracking numbers, delivery, signature confirmation and postal insurance included in the shipping cost.</p>

                              <p>Use www.usps.com to check your package status.</p>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading panel-help" role="tab" id="headingTwo">
                           <h4 class="panel-title">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">• Change quantities or cancel my order?</a>
                           </h4>
                        </div>
                         <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                           <div class="panel-body">
                              <p>Once your order has been processed successfully it is sent electronically to our fulfillment center. This third party fulfillment center is fully automated. Once the order is placed the fulfillment center receives the details of your order, and an electronic fulfillment system similar to a robot selects the correct product and amount as stipulated by the order. A shipping label is then printed and a tracking number is issued before, packages are then assembled and sent to the receiving department. There www.usps.com picks up the packages Monday through Friday.</p>

                              <p>Once the order has been processed, we cannot stop the process. Nor can we cancel orders, redirect them to a different address, or add or change order details or product amounts. The fulfillment center normally completes its process within five minutes after you place your order. Physically attempting to pick your order out and modify it or stop it is not practical or acceptable under our contract with the fulfillment center. Once placed, orders cannot be changed, modified or cancelled. This was agreed upon at the time of purchase when you clicked on “I agree to the terms and conditions” at the time the order was placed.</p>

                              <p>If you wish to add more products to your order, you will have to issue a new order. The existing order cannot be modified from its original state once it has been processed.</p>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default">
                        <div class="panel-heading panel-help" role="tab" id="headingtree">
                           <h4 class="panel-title">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsetree" aria-expanded="false" aria-controls="collapsetree">• How do I track my order?</a>
                           </h4>
                        </div>
                         <div id="collapsetree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtree">
                           <div class="panel-body">
                              <p> Once a tracking number has been assigned to your package it automatically sends an email with your tracking information. In the event you don't receive an email with tracking information you can call us at 1-855-281-8098 (US, Canada or Mexico) or 214-446-0158 (International). You can also email us at support@phen375.com.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="panel panel-default">
                         <div class="panel-heading panel-help" role="tab" id="headingfour">
                           <h4 class="panel-title">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">• Lost Packages. </a>
                           </h4>
                        </div>
                        <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                           <div class="panel-body">
                              <p>All of the packages, international or domestic, are assigned a tracking number from either www.usps.com (US, Canada or Mexico) www.royalmail.com (International Packages) systems.</p>

                              <p>Upon delivery, each package will have a confirmation of arrival from the client or foreign postal service used to deliver the package. .</p>

                              <p>Each package is insured. If lost, the package will be reshipped to the receiver or importer, upon confirmation.</p>

                              <p>We cannot issue refunds on lost shipments; the insurance coverage we use specifically stipulates we have to reship to collect on the missing, lost or stolen package.</p>

                              <p>The insurance company does not allow us to file a claim before 30 days have passed. This is to avoid people attempting to defraud the postal insurance system and claim a lost or stolen package to receive a refund while already in possession of the package.
                              Our Support Staff reserves the right to authorize a reshipment and classify a package as “Lost” to the insurance company if there has not been now notice of delivery passed 30 days.</p>
                              <p>The postal system at times is not updated accurately due to high volume or lost data, but usually updates within 30 days.</p>
                              <p>Most missing packages are found within this time-frame and have their tracking information updated accordingly. It is a federal crime to commit mail or insurance fraud. To avoid any possible issues we are complying with the postal and insurance regulations as required by law. If a customer needs help regarding a missing package they should contact our support staff for further assistance. All claims for lost packages must be submitted within a 90-day period.</p>
                           </div>
                        </div>
                     </div>

                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#coll5">• Missing Items.</a>
                           </h4>
                        </div>
                        <div id="coll5" class="panel-collapse collapse">
                           <div class="panel-body">
                              <p>Although our third-party fulfillment center and warehouse is a fully automated world class facility, we cannot exclude the possibility of errors with the correct amount of bottle being shipped.</p>

                              <p>If you received a package with a missing item, the support team reserves the right to authorize a reshipment on the missing quantity at their discretion.</p>

                              <p>All products purchased are backed by the manufacturer’s warranty first and foremost, therefore we must validate this claim directly to the manufacture. The claim must be documented by the customer/recipient via a support ticket.</p>

                              <p>Additional proof must be required, including:</p>
                              <ul>
                                 <li>Description of the package as it was received. </li>
                                 <li>Photograph of the exterior of the package as well as the interior of the package.</li>
                              </ul>
                              <p>All missing product claims must be made within the first 48 hours of reception.</p>

                           </div>
                        </div>
                     </div>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#coll5">• Damaged Packages.</a>
                           </h4>
                        </div>
                        <div id="coll5" class="panel-collapse collapse">
                           <div class="panel-body">
                              <p>If a package arrived with either unsealed or with a broken seal, it should be returned upon arrival. We will proceed to reship the package. The pertinent postal service is responsible for: Returning the original package back to the sender/distribution sender if it has been damaged. (Refer to Return policy)</p><p>
                              Assuring the perfect state of the package during transit as well as delivering it successfully (in its original state) to its destination. Furthermore, if the package doesn't show any signs of its integrity having been compromised but the product itself is damaged and is inadequate for human ingestion, the customer must contact our support team in order to file claim. There will be a 7-day time-frame to report damaged products.(The time-frame could be extended depending on the quantity ordered. )</p><p>
                              All products purchased are backed by the manufacturer’s warranty first and foremost, therefore we must validate this claim directly to the manufacture. The claim must be documented by the customer/recipient via a support ticket.</p>

                              <p>Additional proof must be required, including:</p>
                              <ul>
                              <li>Description of the package as it was received.</li>
                              <li>Photograph of the exterior of the package.</li>
                              </ul>

                              <p>All products purchased are backed by 45-day manufacturer's warranty.</p>

                           </div>
                        </div>
                     </div>
                  </div>
               <!--next-->

               <!--next-->
               <h4>International Shipping</h4>
               <div class="panel-group" id="accordion2">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a data-toggle="collapse" data-parent="#accordion2" href="#coll10">• Do you ship to my country?</a>
                        </h4>
                     </div>
                     <div id="coll10" class="panel-collapse collapse in">
                        <div class="panel-body">
                           <p>In most cases, yes. There are only a few exceptions. Because we ship from Dallas, Texas, U.S., and use the United States Postal Service, we cannot ship to any country that is currently on an embargoed countries list or where our product is prohibited.</p>

                           <p>Embargoed countries: Burma (Myanmar), the Balkans, Cuba, Iran, Iraq, Libya, North Korea, Liberia, Syria, Somalia, Sudan, Rwanda, Zimbabwe, Sudan, Zaire, Democratic Republic of the Congo, and Palestinian territories.</p>

                           <strong>Countries where Phen375 is not allowed</strong>
                           <p>Only know country where phen375 is considered a party drug or a prescription is required. New Zealand.</p>

                           <strong>My country is not listed in your shipping destinations.</strong>
                           <p>If your country is not listed on in the shipping countries list, first make sure the spelling or abbreviation is correct. In some instances we have seen countries listed but using different abbreviations. If your country is not embargoed and is not listed then you should contact support@phen375.com they will help you resolve the error and we will add your country to our approved countries list located in the shopping cart.</p>

                           <strong>International order status</strong>
                           <p>When you created your order you also created an account. You can log in to your account to see your current order status. To log in to your account you will need the email address and password that you used at the time of your order. If you don’t remember your password you can choose the forgotten password option and a new password will be sent to the email address listed for your account. Once your order has a tracking number, you can track it by using www.usps.com, or the web tracking services of the postal service providing priority mail services in your country. In some countries, private couriers are contracted to provide priority mail services.</p>

                           <strong>When will my order ship and what are my shipping charges?</strong>
                           <p>Typically orders are sent to shipping for dispatch at 11 a.m. each business day. If your order does not ship the same day you placed it, this is due to USPS’s pick-up cut-off times; your package will ship the next business day. (For example, if you order at 1:30 p.m. on a Friday, and the following Monday is a holiday, then the next shipping day would be Tuesday.) All international orders are shipped using www.usps.com priority international mail. All shipments have tracking numbers, delivery, signature confirmation and postal insurance included in the shipping cost. Set standard international fees are shown in the shopping cart at the time of purchase, depending on the country your package is shipping to. For current pricing, simply place an order; you do not have to complete your order, but you can set up your shipping destination and that will reflect the current international pricing for your specific destination.</p>

                           <strong>Go to www.usps.com to check your package status.</strong><br><br>
                           <strong>What is the transit time for international shipping?</strong>
                           <p>All international orders are shipped using www.usps.com priority international mail. . All shipments have tracking numbers, delivery, signature confirmation and postal insurance included in the shipping cost. Priority mail service is an express service but is dependent upon customs clearance within your country. Each country’s customs operates differently and independently from the postal service. Typical transit time for international orders is 5 to 10 business days. You should always check your order status using your tracking number. In most cases you can see the customs progression and movements within your country’s customs and local postal system. Use www.usps.com to check your package status.</p>

                           <strong>Importing internationally.</strong>
                           <p>Orders are exported by Phen375.com to you, the importer. We have complied with all legal documents required to export your order to you via the United States Postal Service. Once the product is shipped and leaves our warehouse or jurisdiction, the responsibility then falls upon the importer. Since each country is different and each country’s customs operates independently, we are unable to offer advice on importing into your country. We do not, however, ship to any known problem country. If your country is listed in the shopping cart then our exports are being received and no known problems exist.</p>

                           <p>All duties, taxes, additional shipping fees or special permits required by your country’s customs for importation are your responsibility as an importer. If your customs holds the product, you are responsible since you are the importer. You will need to resolve any issues with your local customs. This is highly unlikely but it is possible, as there are hundreds of different countries. Postal and customs laws can change at any time within each country.</p>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a data-toggle="collapse" data-parent="#accordion2" href="#coll11">• When will my order ship and what are my shipping charges?</a>
                        </h4>
                     </div>
                     <div id="coll11" class="panel-collapse collapse">
                       <div class="panel-body">
							<p>All packages will be shipped within 24 to 48 hours after order is placed.</p>
							<p>3 - 5 business days domestic packages and 5 - 15 business days for international packages.</p>
							<p>Shipping charges depend on your location. This can be verified in the order checkout page. It will also vary depending on if Standard or Insured shipping is selected.</p>
                        </div>
                     </div>
                  </div>
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a data-toggle="collapse" data-parent="#accordion2" href="#coll12">• What is the return policy?</a>
                        </h4>
                     </div>
                     <div id="coll12" class="panel-collapse collapse">
                        <div class="panel-body">
                           <p>You can read our return policy here: <a href=
                              "return-policy.html" target="_blank">Return Policy</a></p>
                        </div>
                     </div>
                  </div>
               </div>
               <!--next-->


               <!--next-->
               <h4> Pricing and billing</h4>
               <div class="panel-group" id="accordion4">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a data-toggle="collapse" data-parent="#accordion4" href="#coll17">• Do I have to pay sales tax? </a>
                        </h4>
                     </div>
                     <div id="coll17" class="panel-collapse collapse in">
                        <div class="panel-body">

                           <h1>Credit card Billing</h1>
                           <strong>Do I have to pay sales tax or VAT?</strong>
                           <p>Typically, no. Since we are a multinational company we use graphical user interface (GUI) technology to determine your IP address, which would give us your tax jurisdiction. By using this technology we can then charge you from an opposing tax jurisdiction that does not require sales tax or VAT if you are not locally making a purchase. We also adjust the shipping warehouse used for your shipment to further avoid any taxation issues. This allows us to keep prices low and typically the international shipping fees are much lower than local shipping and sales tax or VAT would be.</p>

                           <strong>I have a question about my charges</strong>
                           <p>Click the Login link on the left-hand side of our site to review your order history. You may compare your order history on our website with your financial records. If you have further questions or concerns, please contact customer service for assistance. Any charge on your credit card will show www.phen375.com as the descriptor or business that charged your credit card and should be displayed on your credit card statement along with the customer service number.</p>

                           <strong>I need a copy of my receipt/invoice</strong>
                           <p>Click the Login link on the left-hand side of our site to log in to your account. There, you can then print invoices.</p>

                           <strong>When will my credit card be charged? </strong>
                           <p>The shopping cart first requests authorization from your bank to verify the funds are available. Next, it verifies the address and other security measures to verify that your credit card details match what the bank has on file for the client. If all the security measures are met, the charge is then locked and your credit card is usually debited instantly, but the timing can depend on your credit card issuing bank’s posting procedures.</p>

                           <strong>Why do I see several charges on my account?</strong>
                           <p>Credit card charges will often show almost immediately. The shopping cart first requests authorization from your bank to verify the funds are available. Next, it verifies the address and other security measures to verify that your credit card details match what the bank has on file for the client. If there are any problems with that information the cart may reject the order even though your credit card company approved the charge. This is usually why you may see several charges, but they are only authorizations and not completed charges. The authorization was approved but the charge was declined by our shopping cart security. If you make several attempts it will show on your account. They should be removed within no more than 24 hours and normally the next business day you will notice they are not present. If you are concerned then you can also contact support@phen375.com.</p>

                           <strong>When will my credit show on my card?</strong>
                           <p>Credits are usually processed within 24 hours. If you are cancelling your order before it has been sent to the shipping department for dispatch, then you should receive the credit within that 24-hour period. Some credit card companies do not post credit immediately and can take 7-10 business days. If that is the case with your credit card issuing bank it has nothing to do with us. Our credit card processor returns the credit to the issuing bank within 24 hours.</p>

                           <strong>Credit on returned orders</strong>
                           <p>Please see the Return policy section of this site. The credit will depend on several factors, including the condition of product that was returned; as well, handling, inspection and restocking fees all apply to returned products. Approved returns are credited once the returned report has been received and approved; upon approval your credit will usually take a maximum of 7-10 business days.</p>
                        </div>
                     </div>
                  </div>

               <!--next-->
               <h4>Additional upports details</h4>
               <div class="panel-group" id="accordion6">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           • <a href="contact-us.html">How do I contact you? Click here</a>                        </h4>
                     </div>
                  </div>
               </div>
            </div>
            <!-- /.feature-text -->
			&nbsp;
            <hr>


             <div class="row last-banner">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <img src="http://www.maxslim.store/wp-content/uploads/2018/05/bt-sl-1.png" class="img-responsive center-block" alt="Banner Image">
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h2 class="blue font40">
                    <span class="special-font extra-bold block-span">START YOUR JOURNEY TODAY! TRY PHEN375!</span>
                </h2>
                <h3 class="bold">No Prescription Required!</h3>
                <a href="http://www.maxslim.store/shop" class="button button-links">Rush My Order</a>
                <div style="margin-top: 30px;">
                    <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                        <img name="trustseal" alt="Security Seals" style="border: 0; width: 120px;" src="<?php echo get_theme_file_uri(); ?>/assets/def/img/6121-lg.gif" />
                    </a>
                </div>

                <span class="block-span">
                                      </span>
            </div>
           <!--  <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-12 no-padd">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/fad.png" class="img-responsive seal-img" alt="Made in a FDA Approved Facility">
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in a FDA</span>
                        <span class="dark-blue block-span special-font bold">Approved Facility</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img2.png" class="img-responsive seal-img" alt="100% Quality Guaranteed">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">100% Quality</span>
                        <span class="dark-blue block-span special-font bold">Guaranteed</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 seals">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <img src="https://dc411ibrlpprl.cloudfront.net/newphen375/img3.png" class="img-responsive seal-img" alt="Made in USA">
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 seal-text">
                        <span class="dark-blue block-span special-font bold">Made in</span>
                        <span class="dark-blue block-span special-font bold">USA</span>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="col-md-8 col-md-offset-4 col-sm-8 col-sm-offset-4 col-xs-12">
            <div class="row">
                <!-- <div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-6 seals">
                                            <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
                            <tr>
                                <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.phen375.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
                                </td>
                            </tr>
                        </table>
                                    </div> -->

                <!-- <div class="col-md-12 col-sm-12 col-xs-1 seals">
                    <center>
                        <a name="trustlink" href="http://secure.trust-guard.com/security/6121" rel="nofollow" target="_blank" onclick="var nonwin=navigator.appName!='Microsoft Internet Explorer'?'yes':'no'; window.open(this.href.replace(/https?/, 'https'),'welcome','location='+nonwin+',scrollbars=yes,width=517,height='+screen.availHeight+',menubar=no,toolbar=no'); return false;" oncontextmenu="var d = new Date(); alert('Copying Prohibited by Law - This image and all included logos are copyrighted by trust-guard \251 '+d.getFullYear()+'.'); return false;" >
                            <img name="trustseal" alt="Security Seals" style="border: 0; width: 160px;" src="//dw26xg4lubooo.cloudfront.net/seals/stacked/6121-lg.gif" />
                        </a>
                    </center>
                </div> -->
            </div>
        </div>
</div>            <div class="spacer"></div>
         </div>
      </div>
   </div>						<!-- End Content -->

 <?php get_footer( 'home' ); ?>