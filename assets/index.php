<?php get_header(); ?>
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="posts">
					<div class="post-contaier">
						<?php while ( have_posts() ) : the_post(); ?>
						<div class="post">
							<h1><?php the_title( $before = '', $after = '', $echo = true ); ?></h1>
							<p><?php the_content( $more_link_text = null, $strip_teaser = false ); ?></p>
						</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				
			</div>
		</div>
	</div>
<?php get_footer(); ?>